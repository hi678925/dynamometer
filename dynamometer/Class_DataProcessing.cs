﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dynamometer
{
    static class Class_DataProcessing
    {
        public static double[] MovingAverage(double[] input, int size)
        {
            List<double> _output = new List<double>();
            double _sum = 0;
            for (int i = 0; i < input.Length - size + 1; i++)
            {
                _sum = 0;
                for (int j = i; j < i + size; j++)
                {
                    _sum += input[j];
                }
                _output.Add(_sum / size);
            }
            return _output.ToArray();
        }
        public static void AddCRC(ref byte[] byteData)
        {
            byte[] crc = CalcCRC(byteData, byteData.Length);
            Array.Resize(ref byteData, byteData.Length + 2);
            byteData[byteData.Length - 2] = crc[0];
            byteData[byteData.Length - 1] = crc[1];
        }
        public static byte[] CalcCRC(byte[] byteData, int byteLength)
        {
            byte[] CRC = new byte[2];

            UInt16 wCrc = 0xFFFF;
            for (int i = 0; i < byteLength; i++)
            {
                wCrc ^= Convert.ToUInt16(byteData[i]);
                for (int j = 0; j < 8; j++)
                {
                    if ((wCrc & 0x0001) == 1)
                    {
                        wCrc >>= 1;
                        wCrc ^= 0xA001;//異或多項式
                    }
                    else
                    {
                        wCrc >>= 1;
                    }
                }
            }
            CRC[1] = (byte)((wCrc & 0xFF00) >> 8);//高位在後
            CRC[0] = (byte)(wCrc & 0x00FF);       //低位在前

            return CRC;
        }

        public static bool CheckCRC(byte[] Data, byte[] CRC)
        {
            bool check_result;

            if (Data[Data.Length - 2] == CRC[0] && Data[Data.Length -1] == CRC[1])  //check CRC
                check_result = true;
            else
                check_result = false;

            return check_result;
        }
    }

}
