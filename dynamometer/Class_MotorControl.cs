﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;

namespace dynamometer
{
    public  class Class_MotorControl
    {
        private  string _cmdDeviceNum = "01";

        private  string _cmdRunHeader = "06040E0027";
        private  string _cmdStopHeader = "06040E0036";
        private  string _cmdSpeedSp1Header = "100112";
        private  string _cmdSpeedSp3Header = "100116";
        private  string _cmdTorqueTQ1Header = "060118";
        private  string _cmdTorqueTQ3Header = "06011C";
        private  string _cmdSCurveTaccHeader = "060144";
        private  string _cmdSCurveTdecHeader = "060146";
        private  string _cmdSCurveTslHeader = "060148";

        private  SerialPort _serialPort;

        public enum SpeedNum
        {
            SP1 = 0,
            SP3,
        }
        public enum TorqueNum
        {
            TQ1 = 0,
            TQ3,
        }

        public Class_MotorControl() { }
        public Class_MotorControl(SerialPort serialPort = null)
        {
            _serialPort = serialPort;
        }

        public bool Run(SerialPort serialPort = null)
        {
            if (serialPort == null)
                serialPort = _serialPort;
            if (SerialPortIsReady(serialPort))
            {
                bool result = false;
                result = SendModbus8byte(serialPort, _cmdDeviceNum, _cmdRunHeader);
                Thread.Sleep(50);
                return result;
            }
            return false;
        }
        public bool Stop(SerialPort serialPort = null)
        {
            if (serialPort == null)
                serialPort = _serialPort;
            if (SerialPortIsReady(serialPort))
            {
                bool result = false;
                result = SendModbus8byte(serialPort, _cmdDeviceNum, _cmdStopHeader);
                Thread.Sleep(50);
                return result;
            }
            return false;
        }
        public bool SetSpeed(int speed, SpeedNum spNum = SpeedNum.SP1)
        {
            return SetSpeed(_serialPort, speed, spNum);
        }
        public bool SetSpeed(SerialPort serialPort, int speed, SpeedNum spNum = SpeedNum.SP1)
        {
            if (serialPort == null)
                serialPort = _serialPort;
            if (SerialPortIsReady(serialPort))
            {
                bool result = false;
                if (spNum == SpeedNum.SP1)
                    result = SendModbus2Words(serialPort, _cmdDeviceNum, _cmdSpeedSp1Header, speed);
                else if (spNum == SpeedNum.SP3)
                    result = SendModbus2Words(serialPort, _cmdDeviceNum, _cmdSpeedSp3Header, speed);
                Thread.Sleep(50);
                return result;
            }
            return false;
        }
        public bool SetTorquePercent(int torque, TorqueNum tN = TorqueNum.TQ1)
        {
            return SetTorquePercent(_serialPort, torque, tN);
        }
        public bool SetTorquePercent(SerialPort serialPort, int torque, TorqueNum tN = TorqueNum.TQ1)
        {
            if (serialPort == null)
                serialPort = _serialPort;
            if (SerialPortIsReady(serialPort))
            {
                bool result = false;
                if (tN == TorqueNum.TQ1)
                    result = SendModbus8byte(serialPort, _cmdDeviceNum, _cmdTorqueTQ1Header, torque);
                else if (tN == TorqueNum.TQ3)
                    result = SendModbus8byte(serialPort, _cmdDeviceNum, _cmdTorqueTQ3Header, torque);
                Thread.Sleep(50);
                return result;
            }
            return false;
        }
        public bool SetScurve(int tacc, int tdec, int tsl)
        {
            return SetScurve(_serialPort, tacc, tdec, tsl);
        }
        public bool SetScurve(SerialPort serialPort, int tacc, int tdec, int tsl)   // ms
        {
            if (serialPort == null)
                serialPort = _serialPort;
            if (SerialPortIsReady(serialPort))
            {
                int delay = 50;
                bool result = false;

                result = SendModbus8byte(serialPort, _cmdDeviceNum, _cmdSCurveTaccHeader, tacc);
                if (!result) return false;
                Thread.Sleep(delay);
                result = SendModbus8byte(serialPort, _cmdDeviceNum, _cmdSCurveTdecHeader, tdec);
                if (!result) return false;
                Thread.Sleep(delay);
                result = SendModbus8byte(serialPort, _cmdDeviceNum, _cmdSCurveTslHeader, tsl);
                if (!result) return false;
                Thread.Sleep(delay);
                return true;
            }
            return false;
        }
        private bool SendModbus8byte(SerialPort serialPort, string deviceNum, string cmdHeader, int param = 0)
        {
            byte[] cmd = new byte[1];
            if (cmdHeader.Length > 7) // > 4byte
                cmd = Class_Utility.Hex2Byte(deviceNum + cmdHeader);
            else
                cmd = Class_Utility.Hex2Byte(deviceNum + cmdHeader + param.ToString("X").PadLeft(4, '0'));

            if (cmd.Length != 6)
                return false;
            else
                Class_DataProcessing.AddCRC(ref cmd);
            try
            {
                serialPort.Write(cmd, 0, 8);
            }
            catch
            {
                return false;
            }
            return true;
        }

        private bool SendModbus2Words(SerialPort serialPort, string deviceNum, string cmdHeader, int param = 0)
        {
            byte[] cmd = new byte[1];
            cmd = Class_Utility.Hex2Byte(deviceNum + cmdHeader + "000204" + param.ToString("X").PadLeft(4, '0') + "0000");

            if (cmd.Length != 11)
                return false;
            else
                Class_DataProcessing.AddCRC(ref cmd);
            try
            {
                serialPort.Write(cmd, 0, 13);
            }
            catch
            {
                return false;
            }
            return true;
        }

        private bool SerialPortIsReady(SerialPort serialPort)
        {
            if (serialPort != null)
            {
                if (serialPort.IsOpen)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
