﻿namespace dynamometer
{
    partial class MainForm
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改這個方法的內容。
        ///
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.monitor_gb = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.Motor_eff_unit = new System.Windows.Forms.Label();
            this.Drive_eff_unit = new System.Windows.Forms.Label();
            this.MotorEff_value = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.DriveEff_value = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.Motor_pwr_unit = new System.Windows.Forms.Label();
            this.speed_unit = new System.Windows.Forms.Label();
            this.torque_unit = new System.Windows.Forms.Label();
            this.MotorPower_value = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Torque_value = new System.Windows.Forms.Label();
            this.Speed_value = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.Drive_pwr_unit = new System.Windows.Forms.Label();
            this.Iw_unit = new System.Windows.Forms.Label();
            this.Vvw_unit = new System.Windows.Forms.Label();
            this.Iu_unit = new System.Windows.Forms.Label();
            this.Vuv_unit = new System.Windows.Forms.Label();
            this.DrivePower_value = new System.Windows.Forms.Label();
            this.Iw_value = new System.Windows.Forms.Label();
            this.Iu_value = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Vvw_value = new System.Windows.Forms.Label();
            this.Vuv_value = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.input_pwr_unit = new System.Windows.Forms.Label();
            this.Idc_unit = new System.Windows.Forms.Label();
            this.Vdc_unit = new System.Windows.Forms.Label();
            this.InputPower_value = new System.Windows.Forms.Label();
            this.Idc_value = new System.Windows.Forms.Label();
            this.Vdc_value = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.manual_set_speed_tb = new System.Windows.Forms.TextBox();
            this.btnCOMM = new System.Windows.Forms.Button();
            this.btnManualControl = new System.Windows.Forms.Button();
            this.btnAutoControl = new System.Windows.Forms.Button();
            this.gbCOMM = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.btnSDconnect = new System.Windows.Forms.Button();
            this.cbSDComportList = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.btnPAconnect = new System.Windows.Forms.Button();
            this.cbPAComportList = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnTMConnect = new System.Windows.Forms.Button();
            this.cbTMComportList = new System.Windows.Forms.ComboBox();
            this.gbManualControl = new System.Windows.Forms.GroupBox();
            this.btnClearAll = new System.Windows.Forms.Button();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.manual_stop_btn = new System.Windows.Forms.Button();
            this.rtbManualLOG = new System.Windows.Forms.RichTextBox();
            this.manual_start_btn = new System.Windows.Forms.Button();
            this.btnSaveFile = new System.Windows.Forms.Button();
            this.btnCatchData = new System.Windows.Forms.Button();
            this.gbGenMode = new System.Windows.Forms.GroupBox();
            this.lbTQunit = new System.Windows.Forms.Label();
            this.btnSetTQ = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.manual_set_torque_tb = new System.Windows.Forms.TextBox();
            this.rbGenModeSeleted = new System.Windows.Forms.RadioButton();
            this.label17 = new System.Windows.Forms.Label();
            this.rbMotorModeSeleted = new System.Windows.Forms.RadioButton();
            this.gbMotorMode = new System.Windows.Forms.GroupBox();
            this.lbSPunit = new System.Windows.Forms.Label();
            this.btnSetSP = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.gbAutoControl = new System.Windows.Forms.GroupBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.btnAutoStop = new System.Windows.Forms.Button();
            this.btnAutoStart = new System.Windows.Forms.Button();
            this.tbSampleTime = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.mointer_refresh_timer = new System.Windows.Forms.Timer(this.components);
            this.lbTMState = new System.Windows.Forms.Label();
            this.lbPAState = new System.Windows.Forms.Label();
            this.lbSDState = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.monitor_gb.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.gbCOMM.SuspendLayout();
            this.gbManualControl.SuspendLayout();
            this.gbGenMode.SuspendLayout();
            this.gbMotorMode.SuspendLayout();
            this.gbAutoControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // monitor_gb
            // 
            this.monitor_gb.Controls.Add(this.groupBox6);
            this.monitor_gb.Controls.Add(this.groupBox5);
            this.monitor_gb.Controls.Add(this.groupBox4);
            this.monitor_gb.Controls.Add(this.groupBox3);
            this.monitor_gb.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.monitor_gb.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.monitor_gb.Location = new System.Drawing.Point(12, 12);
            this.monitor_gb.Margin = new System.Windows.Forms.Padding(4);
            this.monitor_gb.Name = "monitor_gb";
            this.monitor_gb.Padding = new System.Windows.Forms.Padding(4);
            this.monitor_gb.Size = new System.Drawing.Size(1167, 335);
            this.monitor_gb.TabIndex = 0;
            this.monitor_gb.TabStop = false;
            this.monitor_gb.Text = "Monitor";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.Motor_eff_unit);
            this.groupBox6.Controls.Add(this.Drive_eff_unit);
            this.groupBox6.Controls.Add(this.MotorEff_value);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this.DriveEff_value);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.groupBox6.ForeColor = System.Drawing.Color.Yellow;
            this.groupBox6.Location = new System.Drawing.Point(920, 30);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox6.Size = new System.Drawing.Size(220, 281);
            this.groupBox6.TabIndex = 8;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Efficiency";
            // 
            // Motor_eff_unit
            // 
            this.Motor_eff_unit.AutoSize = true;
            this.Motor_eff_unit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Motor_eff_unit.Location = new System.Drawing.Point(125, 134);
            this.Motor_eff_unit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Motor_eff_unit.Name = "Motor_eff_unit";
            this.Motor_eff_unit.Size = new System.Drawing.Size(42, 36);
            this.Motor_eff_unit.TabIndex = 6;
            this.Motor_eff_unit.Text = "%";
            // 
            // Drive_eff_unit
            // 
            this.Drive_eff_unit.AutoSize = true;
            this.Drive_eff_unit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Drive_eff_unit.Location = new System.Drawing.Point(125, 60);
            this.Drive_eff_unit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Drive_eff_unit.Name = "Drive_eff_unit";
            this.Drive_eff_unit.Size = new System.Drawing.Size(42, 36);
            this.Drive_eff_unit.TabIndex = 5;
            this.Drive_eff_unit.Text = "%";
            // 
            // MotorEff_value
            // 
            this.MotorEff_value.AutoSize = true;
            this.MotorEff_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.MotorEff_value.Location = new System.Drawing.Point(8, 132);
            this.MotorEff_value.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.MotorEff_value.Name = "MotorEff_value";
            this.MotorEff_value.Size = new System.Drawing.Size(98, 37);
            this.MotorEff_value.TabIndex = 4;
            this.MotorEff_value.Text = "00.00";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label12.Location = new System.Drawing.Point(8, 32);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(85, 25);
            this.label12.TabIndex = 0;
            this.label12.Text = "Drive Eff";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label14.Location = new System.Drawing.Point(8, 106);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(90, 25);
            this.label14.TabIndex = 2;
            this.label14.Text = "Motor Eff";
            // 
            // DriveEff_value
            // 
            this.DriveEff_value.AutoSize = true;
            this.DriveEff_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.DriveEff_value.Location = new System.Drawing.Point(8, 58);
            this.DriveEff_value.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.DriveEff_value.Name = "DriveEff_value";
            this.DriveEff_value.Size = new System.Drawing.Size(98, 37);
            this.DriveEff_value.TabIndex = 3;
            this.DriveEff_value.Text = "00.00";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.Motor_pwr_unit);
            this.groupBox5.Controls.Add(this.speed_unit);
            this.groupBox5.Controls.Add(this.torque_unit);
            this.groupBox5.Controls.Add(this.MotorPower_value);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.Torque_value);
            this.groupBox5.Controls.Add(this.Speed_value);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.ForeColor = System.Drawing.Color.Lime;
            this.groupBox5.Location = new System.Drawing.Point(673, 30);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(220, 281);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Motor";
            // 
            // Motor_pwr_unit
            // 
            this.Motor_pwr_unit.AutoSize = true;
            this.Motor_pwr_unit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Motor_pwr_unit.Location = new System.Drawing.Point(144, 218);
            this.Motor_pwr_unit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Motor_pwr_unit.Name = "Motor_pwr_unit";
            this.Motor_pwr_unit.Size = new System.Drawing.Size(44, 36);
            this.Motor_pwr_unit.TabIndex = 10;
            this.Motor_pwr_unit.Text = "W";
            // 
            // speed_unit
            // 
            this.speed_unit.AutoSize = true;
            this.speed_unit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.speed_unit.Location = new System.Drawing.Point(116, 135);
            this.speed_unit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.speed_unit.Name = "speed_unit";
            this.speed_unit.Size = new System.Drawing.Size(65, 36);
            this.speed_unit.TabIndex = 9;
            this.speed_unit.Text = "rpm";
            // 
            // torque_unit
            // 
            this.torque_unit.AutoSize = true;
            this.torque_unit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.torque_unit.Location = new System.Drawing.Point(113, 61);
            this.torque_unit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.torque_unit.Name = "torque_unit";
            this.torque_unit.Size = new System.Drawing.Size(68, 36);
            this.torque_unit.TabIndex = 8;
            this.torque_unit.Text = "N.m";
            // 
            // MotorPower_value
            // 
            this.MotorPower_value.AutoSize = true;
            this.MotorPower_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.MotorPower_value.Location = new System.Drawing.Point(9, 215);
            this.MotorPower_value.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.MotorPower_value.Name = "MotorPower_value";
            this.MotorPower_value.Size = new System.Drawing.Size(134, 37);
            this.MotorPower_value.TabIndex = 7;
            this.MotorPower_value.Text = "0000.00";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label9.Location = new System.Drawing.Point(9, 32);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 25);
            this.label9.TabIndex = 0;
            this.label9.Text = "Torque";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label10.Location = new System.Drawing.Point(9, 189);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(122, 25);
            this.label10.TabIndex = 4;
            this.label10.Text = "Motor Power";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label11.Location = new System.Drawing.Point(9, 106);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 25);
            this.label11.TabIndex = 2;
            this.label11.Text = "Speed";
            // 
            // Torque_value
            // 
            this.Torque_value.AutoSize = true;
            this.Torque_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Torque_value.Location = new System.Drawing.Point(9, 58);
            this.Torque_value.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Torque_value.Name = "Torque_value";
            this.Torque_value.Size = new System.Drawing.Size(98, 37);
            this.Torque_value.TabIndex = 5;
            this.Torque_value.Text = "00.00";
            // 
            // Speed_value
            // 
            this.Speed_value.AutoSize = true;
            this.Speed_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Speed_value.Location = new System.Drawing.Point(9, 132);
            this.Speed_value.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Speed_value.Name = "Speed_value";
            this.Speed_value.Size = new System.Drawing.Size(98, 37);
            this.Speed_value.TabIndex = 6;
            this.Speed_value.Text = "00.00";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.Desktop;
            this.groupBox4.Controls.Add(this.Drive_pwr_unit);
            this.groupBox4.Controls.Add(this.Iw_unit);
            this.groupBox4.Controls.Add(this.Vvw_unit);
            this.groupBox4.Controls.Add(this.Iu_unit);
            this.groupBox4.Controls.Add(this.Vuv_unit);
            this.groupBox4.Controls.Add(this.DrivePower_value);
            this.groupBox4.Controls.Add(this.Iw_value);
            this.groupBox4.Controls.Add(this.Iu_value);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.Vvw_value);
            this.groupBox4.Controls.Add(this.Vuv_value);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.Fuchsia;
            this.groupBox4.Location = new System.Drawing.Point(268, 30);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(379, 281);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Output";
            // 
            // Drive_pwr_unit
            // 
            this.Drive_pwr_unit.AutoSize = true;
            this.Drive_pwr_unit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Drive_pwr_unit.Location = new System.Drawing.Point(149, 218);
            this.Drive_pwr_unit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Drive_pwr_unit.Name = "Drive_pwr_unit";
            this.Drive_pwr_unit.Size = new System.Drawing.Size(44, 36);
            this.Drive_pwr_unit.TabIndex = 18;
            this.Drive_pwr_unit.Text = "W";
            // 
            // Iw_unit
            // 
            this.Iw_unit.AutoSize = true;
            this.Iw_unit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Iw_unit.Location = new System.Drawing.Point(309, 131);
            this.Iw_unit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Iw_unit.Name = "Iw_unit";
            this.Iw_unit.Size = new System.Drawing.Size(36, 36);
            this.Iw_unit.TabIndex = 17;
            this.Iw_unit.Text = "A";
            // 
            // Vvw_unit
            // 
            this.Vvw_unit.AutoSize = true;
            this.Vvw_unit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Vvw_unit.Location = new System.Drawing.Point(309, 60);
            this.Vvw_unit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Vvw_unit.Name = "Vvw_unit";
            this.Vvw_unit.Size = new System.Drawing.Size(36, 36);
            this.Vvw_unit.TabIndex = 16;
            this.Vvw_unit.Text = "V";
            // 
            // Iu_unit
            // 
            this.Iu_unit.AutoSize = true;
            this.Iu_unit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Iu_unit.Location = new System.Drawing.Point(121, 134);
            this.Iu_unit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Iu_unit.Name = "Iu_unit";
            this.Iu_unit.Size = new System.Drawing.Size(36, 36);
            this.Iu_unit.TabIndex = 15;
            this.Iu_unit.Text = "A";
            // 
            // Vuv_unit
            // 
            this.Vuv_unit.AutoSize = true;
            this.Vuv_unit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Vuv_unit.Location = new System.Drawing.Point(121, 61);
            this.Vuv_unit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Vuv_unit.Name = "Vuv_unit";
            this.Vuv_unit.Size = new System.Drawing.Size(36, 36);
            this.Vuv_unit.TabIndex = 14;
            this.Vuv_unit.Text = "V";
            // 
            // DrivePower_value
            // 
            this.DrivePower_value.AutoSize = true;
            this.DrivePower_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.DrivePower_value.Location = new System.Drawing.Point(12, 215);
            this.DrivePower_value.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.DrivePower_value.Name = "DrivePower_value";
            this.DrivePower_value.Size = new System.Drawing.Size(134, 37);
            this.DrivePower_value.TabIndex = 13;
            this.DrivePower_value.Text = "0000.00";
            // 
            // Iw_value
            // 
            this.Iw_value.AutoSize = true;
            this.Iw_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Iw_value.Location = new System.Drawing.Point(195, 132);
            this.Iw_value.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Iw_value.Name = "Iw_value";
            this.Iw_value.Size = new System.Drawing.Size(98, 37);
            this.Iw_value.TabIndex = 12;
            this.Iw_value.Text = "00.00";
            // 
            // Iu_value
            // 
            this.Iu_value.AutoSize = true;
            this.Iu_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Iu_value.Location = new System.Drawing.Point(12, 132);
            this.Iu_value.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Iu_value.Name = "Iu_value";
            this.Iu_value.Size = new System.Drawing.Size(98, 37);
            this.Iu_value.TabIndex = 10;
            this.Iu_value.Text = "00.00";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label7.Location = new System.Drawing.Point(195, 32);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 25);
            this.label7.TabIndex = 6;
            this.label7.Text = "Vvw";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label8.Location = new System.Drawing.Point(195, 106);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 25);
            this.label8.TabIndex = 8;
            this.label8.Text = "Iw";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(12, 32);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 25);
            this.label4.TabIndex = 0;
            this.label4.Text = "Vuv";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(12, 189);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 25);
            this.label5.TabIndex = 4;
            this.label5.Text = "Drive Power";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label6.Location = new System.Drawing.Point(12, 106);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 25);
            this.label6.TabIndex = 2;
            this.label6.Text = "Iu";
            // 
            // Vvw_value
            // 
            this.Vvw_value.AutoSize = true;
            this.Vvw_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Vvw_value.Location = new System.Drawing.Point(195, 58);
            this.Vvw_value.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Vvw_value.Name = "Vvw_value";
            this.Vvw_value.Size = new System.Drawing.Size(98, 37);
            this.Vvw_value.TabIndex = 11;
            this.Vvw_value.Text = "00.00";
            // 
            // Vuv_value
            // 
            this.Vuv_value.AutoSize = true;
            this.Vuv_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Vuv_value.Location = new System.Drawing.Point(12, 58);
            this.Vuv_value.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Vuv_value.Name = "Vuv_value";
            this.Vuv_value.Size = new System.Drawing.Size(98, 37);
            this.Vuv_value.TabIndex = 9;
            this.Vuv_value.Text = "00.00";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.input_pwr_unit);
            this.groupBox3.Controls.Add(this.Idc_unit);
            this.groupBox3.Controls.Add(this.Vdc_unit);
            this.groupBox3.Controls.Add(this.InputPower_value);
            this.groupBox3.Controls.Add(this.Idc_value);
            this.groupBox3.Controls.Add(this.Vdc_value);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.groupBox3.Location = new System.Drawing.Point(21, 30);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(220, 281);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Input";
            // 
            // input_pwr_unit
            // 
            this.input_pwr_unit.AutoSize = true;
            this.input_pwr_unit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.input_pwr_unit.Location = new System.Drawing.Point(144, 215);
            this.input_pwr_unit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.input_pwr_unit.Name = "input_pwr_unit";
            this.input_pwr_unit.Size = new System.Drawing.Size(44, 36);
            this.input_pwr_unit.TabIndex = 10;
            this.input_pwr_unit.Text = "W";
            // 
            // Idc_unit
            // 
            this.Idc_unit.AutoSize = true;
            this.Idc_unit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Idc_unit.Location = new System.Drawing.Point(113, 134);
            this.Idc_unit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Idc_unit.Name = "Idc_unit";
            this.Idc_unit.Size = new System.Drawing.Size(36, 36);
            this.Idc_unit.TabIndex = 9;
            this.Idc_unit.Text = "A";
            // 
            // Vdc_unit
            // 
            this.Vdc_unit.AutoSize = true;
            this.Vdc_unit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Vdc_unit.Location = new System.Drawing.Point(113, 60);
            this.Vdc_unit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Vdc_unit.Name = "Vdc_unit";
            this.Vdc_unit.Size = new System.Drawing.Size(36, 36);
            this.Vdc_unit.TabIndex = 8;
            this.Vdc_unit.Text = "V";
            // 
            // InputPower_value
            // 
            this.InputPower_value.AutoSize = true;
            this.InputPower_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.InputPower_value.Location = new System.Drawing.Point(8, 214);
            this.InputPower_value.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.InputPower_value.Name = "InputPower_value";
            this.InputPower_value.Size = new System.Drawing.Size(134, 37);
            this.InputPower_value.TabIndex = 7;
            this.InputPower_value.Text = "0000.00";
            // 
            // Idc_value
            // 
            this.Idc_value.AutoSize = true;
            this.Idc_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Idc_value.Location = new System.Drawing.Point(8, 132);
            this.Idc_value.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Idc_value.Name = "Idc_value";
            this.Idc_value.Size = new System.Drawing.Size(98, 37);
            this.Idc_value.TabIndex = 6;
            this.Idc_value.Text = "00.00";
            // 
            // Vdc_value
            // 
            this.Vdc_value.AutoSize = true;
            this.Vdc_value.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Vdc_value.Location = new System.Drawing.Point(8, 58);
            this.Vdc_value.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Vdc_value.Name = "Vdc_value";
            this.Vdc_value.Size = new System.Drawing.Size(98, 37);
            this.Vdc_value.TabIndex = 5;
            this.Vdc_value.Text = "00.00";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(8, 32);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Vdc";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(8, 189);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 25);
            this.label3.TabIndex = 4;
            this.label3.Text = "Input Power";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(8, 106);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 25);
            this.label2.TabIndex = 2;
            this.label2.Text = "Idc";
            // 
            // manual_set_speed_tb
            // 
            this.manual_set_speed_tb.Location = new System.Drawing.Point(140, 70);
            this.manual_set_speed_tb.Margin = new System.Windows.Forms.Padding(4);
            this.manual_set_speed_tb.Multiline = true;
            this.manual_set_speed_tb.Name = "manual_set_speed_tb";
            this.manual_set_speed_tb.Size = new System.Drawing.Size(100, 36);
            this.manual_set_speed_tb.TabIndex = 3;
            this.manual_set_speed_tb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnlyNumeric_KeyPress);
            // 
            // btnCOMM
            // 
            this.btnCOMM.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCOMM.Location = new System.Drawing.Point(1033, 362);
            this.btnCOMM.Margin = new System.Windows.Forms.Padding(4);
            this.btnCOMM.Name = "btnCOMM";
            this.btnCOMM.Size = new System.Drawing.Size(145, 46);
            this.btnCOMM.TabIndex = 1;
            this.btnCOMM.Text = "Communication";
            this.btnCOMM.UseVisualStyleBackColor = true;
            this.btnCOMM.Click += new System.EventHandler(this.btnCOMM_Click);
            // 
            // btnManualControl
            // 
            this.btnManualControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnManualControl.Location = new System.Drawing.Point(1033, 419);
            this.btnManualControl.Margin = new System.Windows.Forms.Padding(4);
            this.btnManualControl.Name = "btnManualControl";
            this.btnManualControl.Size = new System.Drawing.Size(145, 46);
            this.btnManualControl.TabIndex = 3;
            this.btnManualControl.Text = "Manual Control";
            this.btnManualControl.UseVisualStyleBackColor = true;
            this.btnManualControl.Click += new System.EventHandler(this.btnManualControl_Click);
            // 
            // btnAutoControl
            // 
            this.btnAutoControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAutoControl.Location = new System.Drawing.Point(1033, 475);
            this.btnAutoControl.Margin = new System.Windows.Forms.Padding(4);
            this.btnAutoControl.Name = "btnAutoControl";
            this.btnAutoControl.Size = new System.Drawing.Size(145, 46);
            this.btnAutoControl.TabIndex = 4;
            this.btnAutoControl.Text = "Auto Control";
            this.btnAutoControl.UseVisualStyleBackColor = true;
            this.btnAutoControl.Click += new System.EventHandler(this.btnAutoControl_Click);
            // 
            // gbCOMM
            // 
            this.gbCOMM.Controls.Add(this.label16);
            this.gbCOMM.Controls.Add(this.btnSDconnect);
            this.gbCOMM.Controls.Add(this.cbSDComportList);
            this.gbCOMM.Controls.Add(this.label15);
            this.gbCOMM.Controls.Add(this.btnPAconnect);
            this.gbCOMM.Controls.Add(this.cbPAComportList);
            this.gbCOMM.Controls.Add(this.label13);
            this.gbCOMM.Controls.Add(this.btnTMConnect);
            this.gbCOMM.Controls.Add(this.cbTMComportList);
            this.gbCOMM.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.gbCOMM.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gbCOMM.Location = new System.Drawing.Point(12, 350);
            this.gbCOMM.Margin = new System.Windows.Forms.Padding(4);
            this.gbCOMM.Name = "gbCOMM";
            this.gbCOMM.Padding = new System.Windows.Forms.Padding(4);
            this.gbCOMM.Size = new System.Drawing.Size(1007, 500);
            this.gbCOMM.TabIndex = 5;
            this.gbCOMM.TabStop = false;
            this.gbCOMM.Text = "Communication";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(43, 199);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(150, 29);
            this.label16.TabIndex = 15;
            this.label16.Text = "Servo Drive :";
            // 
            // btnSDconnect
            // 
            this.btnSDconnect.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnSDconnect.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSDconnect.Location = new System.Drawing.Point(515, 195);
            this.btnSDconnect.Margin = new System.Windows.Forms.Padding(4);
            this.btnSDconnect.Name = "btnSDconnect";
            this.btnSDconnect.Size = new System.Drawing.Size(160, 41);
            this.btnSDconnect.TabIndex = 14;
            this.btnSDconnect.Text = "Connect";
            this.btnSDconnect.UseVisualStyleBackColor = false;
            this.btnSDconnect.Click += new System.EventHandler(this.btnSDconnect_Click);
            // 
            // cbSDComportList
            // 
            this.cbSDComportList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSDComportList.FormattingEnabled = true;
            this.cbSDComportList.Location = new System.Drawing.Point(231, 195);
            this.cbSDComportList.Margin = new System.Windows.Forms.Padding(4);
            this.cbSDComportList.Name = "cbSDComportList";
            this.cbSDComportList.Size = new System.Drawing.Size(255, 37);
            this.cbSDComportList.TabIndex = 13;
            this.cbSDComportList.DropDown += new System.EventHandler(this.cbSDComportList_DropDown);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(43, 129);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(298, 29);
            this.label15.TabIndex = 12;
            this.label15.Text = "Power Analyzer(PW3337) :";
            // 
            // btnPAconnect
            // 
            this.btnPAconnect.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnPAconnect.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnPAconnect.Location = new System.Drawing.Point(677, 124);
            this.btnPAconnect.Margin = new System.Windows.Forms.Padding(4);
            this.btnPAconnect.Name = "btnPAconnect";
            this.btnPAconnect.Size = new System.Drawing.Size(160, 41);
            this.btnPAconnect.TabIndex = 11;
            this.btnPAconnect.Text = "Connect";
            this.btnPAconnect.UseVisualStyleBackColor = false;
            this.btnPAconnect.Click += new System.EventHandler(this.btnPAconnect_Click);
            // 
            // cbPAComportList
            // 
            this.cbPAComportList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPAComportList.FormattingEnabled = true;
            this.cbPAComportList.Location = new System.Drawing.Point(393, 124);
            this.cbPAComportList.Margin = new System.Windows.Forms.Padding(4);
            this.cbPAComportList.Name = "cbPAComportList";
            this.cbPAComportList.Size = new System.Drawing.Size(255, 37);
            this.cbPAComportList.TabIndex = 10;
            this.cbPAComportList.DropDown += new System.EventHandler(this.cbPAComportList_DropDown);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(43, 59);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(319, 29);
            this.label13.TabIndex = 9;
            this.label13.Text = "Torque Meter/Speed Meter :";
            // 
            // btnTMConnect
            // 
            this.btnTMConnect.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnTMConnect.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnTMConnect.Location = new System.Drawing.Point(677, 54);
            this.btnTMConnect.Margin = new System.Windows.Forms.Padding(4);
            this.btnTMConnect.Name = "btnTMConnect";
            this.btnTMConnect.Size = new System.Drawing.Size(160, 41);
            this.btnTMConnect.TabIndex = 8;
            this.btnTMConnect.Text = "Connect";
            this.btnTMConnect.UseVisualStyleBackColor = false;
            this.btnTMConnect.Click += new System.EventHandler(this.btnTMConnect_Click);
            // 
            // cbTMComportList
            // 
            this.cbTMComportList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTMComportList.FormattingEnabled = true;
            this.cbTMComportList.Location = new System.Drawing.Point(393, 54);
            this.cbTMComportList.Margin = new System.Windows.Forms.Padding(4);
            this.cbTMComportList.Name = "cbTMComportList";
            this.cbTMComportList.Size = new System.Drawing.Size(255, 37);
            this.cbTMComportList.TabIndex = 0;
            this.cbTMComportList.DropDown += new System.EventHandler(this.cbTMComportList_DropDown);
            // 
            // gbManualControl
            // 
            this.gbManualControl.Controls.Add(this.btnClearAll);
            this.gbManualControl.Controls.Add(this.label41);
            this.gbManualControl.Controls.Add(this.label40);
            this.gbManualControl.Controls.Add(this.label36);
            this.gbManualControl.Controls.Add(this.label35);
            this.gbManualControl.Controls.Add(this.label34);
            this.gbManualControl.Controls.Add(this.label33);
            this.gbManualControl.Controls.Add(this.label32);
            this.gbManualControl.Controls.Add(this.label31);
            this.gbManualControl.Controls.Add(this.label30);
            this.gbManualControl.Controls.Add(this.label29);
            this.gbManualControl.Controls.Add(this.label28);
            this.gbManualControl.Controls.Add(this.label24);
            this.gbManualControl.Controls.Add(this.label23);
            this.gbManualControl.Controls.Add(this.manual_stop_btn);
            this.gbManualControl.Controls.Add(this.rtbManualLOG);
            this.gbManualControl.Controls.Add(this.manual_start_btn);
            this.gbManualControl.Controls.Add(this.btnSaveFile);
            this.gbManualControl.Controls.Add(this.btnCatchData);
            this.gbManualControl.Controls.Add(this.gbGenMode);
            this.gbManualControl.Controls.Add(this.rbGenModeSeleted);
            this.gbManualControl.Controls.Add(this.label17);
            this.gbManualControl.Controls.Add(this.rbMotorModeSeleted);
            this.gbManualControl.Controls.Add(this.gbMotorMode);
            this.gbManualControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.gbManualControl.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gbManualControl.Location = new System.Drawing.Point(12, 350);
            this.gbManualControl.Margin = new System.Windows.Forms.Padding(4);
            this.gbManualControl.Name = "gbManualControl";
            this.gbManualControl.Padding = new System.Windows.Forms.Padding(4);
            this.gbManualControl.Size = new System.Drawing.Size(1007, 500);
            this.gbManualControl.TabIndex = 6;
            this.gbManualControl.TabStop = false;
            this.gbManualControl.Text = "Manual Control";
            // 
            // btnClearAll
            // 
            this.btnClearAll.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnClearAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.5F);
            this.btnClearAll.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnClearAll.Location = new System.Drawing.Point(177, 258);
            this.btnClearAll.Margin = new System.Windows.Forms.Padding(4);
            this.btnClearAll.Name = "btnClearAll";
            this.btnClearAll.Size = new System.Drawing.Size(141, 45);
            this.btnClearAll.TabIndex = 30;
            this.btnClearAll.Text = "Clear All";
            this.btnClearAll.UseVisualStyleBackColor = false;
            this.btnClearAll.Click += new System.EventHandler(this.btnClearAll_Click);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.ForeColor = System.Drawing.Color.Yellow;
            this.label41.Location = new System.Drawing.Point(877, 312);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(75, 29);
            this.label41.TabIndex = 29;
            this.label41.Text = "Motor";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.ForeColor = System.Drawing.Color.Yellow;
            this.label40.Location = new System.Drawing.Point(812, 312);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(49, 29);
            this.label40.TabIndex = 28;
            this.label40.Text = "Drv";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.ForeColor = System.Drawing.Color.Lime;
            this.label36.Location = new System.Drawing.Point(724, 312);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(55, 29);
            this.label36.TabIndex = 27;
            this.label36.Text = "Pwr";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.ForeColor = System.Drawing.Color.Lime;
            this.label35.Location = new System.Drawing.Point(656, 312);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(57, 29);
            this.label35.TabIndex = 26;
            this.label35.Text = "Spd";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.ForeColor = System.Drawing.Color.Lime;
            this.label34.Location = new System.Drawing.Point(589, 312);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(43, 29);
            this.label34.TabIndex = 25;
            this.label34.Text = "Tq";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.ForeColor = System.Drawing.Color.Fuchsia;
            this.label33.Location = new System.Drawing.Point(499, 312);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(55, 29);
            this.label33.TabIndex = 24;
            this.label33.Text = "Pwr";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.ForeColor = System.Drawing.Color.Fuchsia;
            this.label32.Location = new System.Drawing.Point(433, 312);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(37, 29);
            this.label32.TabIndex = 23;
            this.label32.Text = "Iw";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.ForeColor = System.Drawing.Color.Fuchsia;
            this.label31.Location = new System.Drawing.Point(369, 312);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(32, 29);
            this.label31.TabIndex = 22;
            this.label31.Text = "Iu";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.ForeColor = System.Drawing.Color.Fuchsia;
            this.label30.Location = new System.Drawing.Point(304, 312);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(57, 29);
            this.label30.TabIndex = 21;
            this.label30.Text = "Vvw";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.ForeColor = System.Drawing.Color.Fuchsia;
            this.label29.Location = new System.Drawing.Point(237, 312);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(52, 29);
            this.label29.TabIndex = 20;
            this.label29.Text = "Vuv";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label28.Location = new System.Drawing.Point(149, 312);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(55, 29);
            this.label28.TabIndex = 19;
            this.label28.Text = "Pwr";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label24.Location = new System.Drawing.Point(85, 312);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(45, 29);
            this.label24.TabIndex = 18;
            this.label24.Text = "Idc";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label23.Location = new System.Drawing.Point(16, 312);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(54, 29);
            this.label23.TabIndex = 17;
            this.label23.Text = "Vdc";
            // 
            // manual_stop_btn
            // 
            this.manual_stop_btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.manual_stop_btn.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.manual_stop_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.5F);
            this.manual_stop_btn.ForeColor = System.Drawing.Color.Black;
            this.manual_stop_btn.Location = new System.Drawing.Point(529, 258);
            this.manual_stop_btn.Margin = new System.Windows.Forms.Padding(4);
            this.manual_stop_btn.Name = "manual_stop_btn";
            this.manual_stop_btn.Size = new System.Drawing.Size(187, 45);
            this.manual_stop_btn.TabIndex = 16;
            this.manual_stop_btn.Text = "Stop";
            this.manual_stop_btn.UseVisualStyleBackColor = false;
            this.manual_stop_btn.Click += new System.EventHandler(this.manual_stop_btn_Click);
            // 
            // rtbManualLOG
            // 
            this.rtbManualLOG.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbManualLOG.Location = new System.Drawing.Point(17, 346);
            this.rtbManualLOG.Margin = new System.Windows.Forms.Padding(4);
            this.rtbManualLOG.Name = "rtbManualLOG";
            this.rtbManualLOG.Size = new System.Drawing.Size(971, 128);
            this.rtbManualLOG.TabIndex = 15;
            this.rtbManualLOG.Text = "";
            this.rtbManualLOG.TextChanged += new System.EventHandler(this.rtbManualLOG_TextChanged);
            // 
            // manual_start_btn
            // 
            this.manual_start_btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.manual_start_btn.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.manual_start_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.5F);
            this.manual_start_btn.ForeColor = System.Drawing.Color.Black;
            this.manual_start_btn.Location = new System.Drawing.Point(803, 258);
            this.manual_start_btn.Margin = new System.Windows.Forms.Padding(4);
            this.manual_start_btn.Name = "manual_start_btn";
            this.manual_start_btn.Size = new System.Drawing.Size(187, 45);
            this.manual_start_btn.TabIndex = 14;
            this.manual_start_btn.Text = "Start";
            this.manual_start_btn.UseVisualStyleBackColor = false;
            this.manual_start_btn.Click += new System.EventHandler(this.manual_start_btn_Click);
            // 
            // btnSaveFile
            // 
            this.btnSaveFile.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnSaveFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.5F);
            this.btnSaveFile.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSaveFile.Location = new System.Drawing.Point(336, 258);
            this.btnSaveFile.Margin = new System.Windows.Forms.Padding(4);
            this.btnSaveFile.Name = "btnSaveFile";
            this.btnSaveFile.Size = new System.Drawing.Size(141, 45);
            this.btnSaveFile.TabIndex = 13;
            this.btnSaveFile.Text = "Save File";
            this.btnSaveFile.UseVisualStyleBackColor = false;
            this.btnSaveFile.Click += new System.EventHandler(this.btnSaveFile_Click);
            // 
            // btnCatchData
            // 
            this.btnCatchData.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCatchData.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.5F);
            this.btnCatchData.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnCatchData.Location = new System.Drawing.Point(17, 258);
            this.btnCatchData.Margin = new System.Windows.Forms.Padding(4);
            this.btnCatchData.Name = "btnCatchData";
            this.btnCatchData.Size = new System.Drawing.Size(141, 45);
            this.btnCatchData.TabIndex = 12;
            this.btnCatchData.Text = "Catch Data";
            this.btnCatchData.UseVisualStyleBackColor = false;
            this.btnCatchData.Click += new System.EventHandler(this.btnCatchData_Click);
            // 
            // gbGenMode
            // 
            this.gbGenMode.Controls.Add(this.lbTQunit);
            this.gbGenMode.Controls.Add(this.btnSetTQ);
            this.gbGenMode.Controls.Add(this.label19);
            this.gbGenMode.Controls.Add(this.manual_set_torque_tb);
            this.gbGenMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.gbGenMode.ForeColor = System.Drawing.Color.Cyan;
            this.gbGenMode.Location = new System.Drawing.Point(529, 94);
            this.gbGenMode.Margin = new System.Windows.Forms.Padding(4);
            this.gbGenMode.Name = "gbGenMode";
            this.gbGenMode.Padding = new System.Windows.Forms.Padding(4);
            this.gbGenMode.Size = new System.Drawing.Size(460, 156);
            this.gbGenMode.TabIndex = 8;
            this.gbGenMode.TabStop = false;
            this.gbGenMode.Text = "Generator Mode";
            this.gbGenMode.EnabledChanged += new System.EventHandler(this.gbGenMode_EnabledChanged);
            // 
            // lbTQunit
            // 
            this.lbTQunit.AutoSize = true;
            this.lbTQunit.Location = new System.Drawing.Point(251, 72);
            this.lbTQunit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbTQunit.Name = "lbTQunit";
            this.lbTQunit.Size = new System.Drawing.Size(57, 29);
            this.lbTQunit.TabIndex = 7;
            this.lbTQunit.Text = "N.m";
            // 
            // btnSetTQ
            // 
            this.btnSetTQ.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnSetTQ.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.5F);
            this.btnSetTQ.ForeColor = System.Drawing.Color.Transparent;
            this.btnSetTQ.Location = new System.Drawing.Point(332, 68);
            this.btnSetTQ.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSetTQ.Name = "btnSetTQ";
            this.btnSetTQ.Size = new System.Drawing.Size(105, 40);
            this.btnSetTQ.TabIndex = 6;
            this.btnSetTQ.Text = "Set";
            this.btnSetTQ.UseVisualStyleBackColor = true;
            this.btnSetTQ.Click += new System.EventHandler(this.btnSetTQ_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(24, 74);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(110, 29);
            this.label19.TabIndex = 3;
            this.label19.Text = "Torque : ";
            // 
            // manual_set_torque_tb
            // 
            this.manual_set_torque_tb.Location = new System.Drawing.Point(140, 70);
            this.manual_set_torque_tb.Margin = new System.Windows.Forms.Padding(4);
            this.manual_set_torque_tb.Name = "manual_set_torque_tb";
            this.manual_set_torque_tb.Size = new System.Drawing.Size(100, 34);
            this.manual_set_torque_tb.TabIndex = 2;
            // 
            // rbGenModeSeleted
            // 
            this.rbGenModeSeleted.AutoSize = true;
            this.rbGenModeSeleted.ForeColor = System.Drawing.Color.Cyan;
            this.rbGenModeSeleted.Location = new System.Drawing.Point(264, 42);
            this.rbGenModeSeleted.Margin = new System.Windows.Forms.Padding(4);
            this.rbGenModeSeleted.Name = "rbGenModeSeleted";
            this.rbGenModeSeleted.Size = new System.Drawing.Size(142, 33);
            this.rbGenModeSeleted.TabIndex = 11;
            this.rbGenModeSeleted.TabStop = true;
            this.rbGenModeSeleted.Text = "Generator";
            this.rbGenModeSeleted.UseVisualStyleBackColor = true;
            this.rbGenModeSeleted.CheckedChanged += new System.EventHandler(this.rbGenModeSeleted_CheckedChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(35, 45);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(87, 29);
            this.label17.TabIndex = 10;
            this.label17.Text = "Mode :";
            // 
            // rbMotorModeSeleted
            // 
            this.rbMotorModeSeleted.AutoSize = true;
            this.rbMotorModeSeleted.ForeColor = System.Drawing.Color.Lime;
            this.rbMotorModeSeleted.Location = new System.Drawing.Point(151, 42);
            this.rbMotorModeSeleted.Margin = new System.Windows.Forms.Padding(4);
            this.rbMotorModeSeleted.Name = "rbMotorModeSeleted";
            this.rbMotorModeSeleted.Size = new System.Drawing.Size(96, 33);
            this.rbMotorModeSeleted.TabIndex = 9;
            this.rbMotorModeSeleted.TabStop = true;
            this.rbMotorModeSeleted.Text = "Motor";
            this.rbMotorModeSeleted.UseVisualStyleBackColor = true;
            this.rbMotorModeSeleted.CheckedChanged += new System.EventHandler(this.rbMotorModeSeleted_CheckedChanged);
            // 
            // gbMotorMode
            // 
            this.gbMotorMode.AccessibleName = "";
            this.gbMotorMode.Controls.Add(this.lbSPunit);
            this.gbMotorMode.Controls.Add(this.btnSetSP);
            this.gbMotorMode.Controls.Add(this.label18);
            this.gbMotorMode.Controls.Add(this.manual_set_speed_tb);
            this.gbMotorMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.gbMotorMode.ForeColor = System.Drawing.Color.Lime;
            this.gbMotorMode.Location = new System.Drawing.Point(17, 94);
            this.gbMotorMode.Margin = new System.Windows.Forms.Padding(4);
            this.gbMotorMode.Name = "gbMotorMode";
            this.gbMotorMode.Padding = new System.Windows.Forms.Padding(4);
            this.gbMotorMode.Size = new System.Drawing.Size(460, 156);
            this.gbMotorMode.TabIndex = 7;
            this.gbMotorMode.TabStop = false;
            this.gbMotorMode.Text = "Motor Mode";
            this.gbMotorMode.EnabledChanged += new System.EventHandler(this.gbMotorMode_EnabledChanged);
            // 
            // lbSPunit
            // 
            this.lbSPunit.AutoSize = true;
            this.lbSPunit.Location = new System.Drawing.Point(251, 72);
            this.lbSPunit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbSPunit.Name = "lbSPunit";
            this.lbSPunit.Size = new System.Drawing.Size(55, 29);
            this.lbSPunit.TabIndex = 5;
            this.lbSPunit.Text = "rpm";
            // 
            // btnSetSP
            // 
            this.btnSetSP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.5F);
            this.btnSetSP.ForeColor = System.Drawing.Color.Black;
            this.btnSetSP.Location = new System.Drawing.Point(332, 68);
            this.btnSetSP.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSetSP.Name = "btnSetSP";
            this.btnSetSP.Size = new System.Drawing.Size(101, 40);
            this.btnSetSP.TabIndex = 4;
            this.btnSetSP.Text = "Set";
            this.btnSetSP.UseVisualStyleBackColor = true;
            this.btnSetSP.Click += new System.EventHandler(this.btnSetSP_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(24, 74);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(103, 29);
            this.label18.TabIndex = 1;
            this.label18.Text = "Speed : ";
            // 
            // gbAutoControl
            // 
            this.gbAutoControl.Controls.Add(this.label39);
            this.gbAutoControl.Controls.Add(this.label38);
            this.gbAutoControl.Controls.Add(this.pictureBox1);
            this.gbAutoControl.Controls.Add(this.label37);
            this.gbAutoControl.Controls.Add(this.label27);
            this.gbAutoControl.Controls.Add(this.label26);
            this.gbAutoControl.Controls.Add(this.label25);
            this.gbAutoControl.Controls.Add(this.panel1);
            this.gbAutoControl.Controls.Add(this.label21);
            this.gbAutoControl.Controls.Add(this.btnAutoStop);
            this.gbAutoControl.Controls.Add(this.btnAutoStart);
            this.gbAutoControl.Controls.Add(this.tbSampleTime);
            this.gbAutoControl.Controls.Add(this.label20);
            this.gbAutoControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.gbAutoControl.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.gbAutoControl.Location = new System.Drawing.Point(12, 350);
            this.gbAutoControl.Margin = new System.Windows.Forms.Padding(4);
            this.gbAutoControl.Name = "gbAutoControl";
            this.gbAutoControl.Padding = new System.Windows.Forms.Padding(4);
            this.gbAutoControl.Size = new System.Drawing.Size(1007, 500);
            this.gbAutoControl.TabIndex = 7;
            this.gbAutoControl.TabStop = false;
            this.gbAutoControl.Text = "Auto Control";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.Red;
            this.label39.Location = new System.Drawing.Point(647, 362);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(143, 25);
            this.label39.TabIndex = 33;
            this.label39.Text = "(Min = 0.2 sec)";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(203, 42);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(55, 29);
            this.label38.TabIndex = 32;
            this.label38.Text = "Unit";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::dynamometer.Properties.Resources.auto_control_value2;
            this.pictureBox1.Location = new System.Drawing.Point(633, 81);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(331, 211);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 25;
            this.pictureBox1.TabStop = false;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(443, 42);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(134, 29);
            this.label37.TabIndex = 31;
            this.label37.Text = "Stable time";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(308, 42);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(115, 29);
            this.label27.TabIndex = 30;
            this.label27.Text = "Step time";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(100, 42);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(74, 29);
            this.label26.TabIndex = 29;
            this.label26.Text = "Value";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(13, 42);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(63, 29);
            this.label25.TabIndex = 28;
            this.label25.Text = "Step";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Location = new System.Drawing.Point(19, 81);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(587, 398);
            this.panel1.TabIndex = 27;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.Cyan;
            this.label21.Location = new System.Drawing.Point(913, 328);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(51, 29);
            this.label21.TabIndex = 26;
            this.label21.Text = "sec";
            // 
            // btnAutoStop
            // 
            this.btnAutoStop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnAutoStop.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnAutoStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAutoStop.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnAutoStop.Location = new System.Drawing.Point(633, 429);
            this.btnAutoStop.Margin = new System.Windows.Forms.Padding(4);
            this.btnAutoStop.Name = "btnAutoStop";
            this.btnAutoStop.Size = new System.Drawing.Size(129, 45);
            this.btnAutoStop.TabIndex = 24;
            this.btnAutoStop.Text = "Stop";
            this.btnAutoStop.UseVisualStyleBackColor = false;
            this.btnAutoStop.Click += new System.EventHandler(this.btnAutoStop_Click);
            // 
            // btnAutoStart
            // 
            this.btnAutoStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnAutoStart.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnAutoStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAutoStart.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnAutoStart.Location = new System.Drawing.Point(837, 429);
            this.btnAutoStart.Margin = new System.Windows.Forms.Padding(4);
            this.btnAutoStart.Name = "btnAutoStart";
            this.btnAutoStart.Size = new System.Drawing.Size(129, 45);
            this.btnAutoStart.TabIndex = 8;
            this.btnAutoStart.Text = "Start";
            this.btnAutoStart.UseVisualStyleBackColor = false;
            this.btnAutoStart.Click += new System.EventHandler(this.btnAutoStart_Click);
            // 
            // tbSampleTime
            // 
            this.tbSampleTime.Location = new System.Drawing.Point(825, 328);
            this.tbSampleTime.Margin = new System.Windows.Forms.Padding(4);
            this.tbSampleTime.Name = "tbSampleTime";
            this.tbSampleTime.Size = new System.Drawing.Size(80, 34);
            this.tbSampleTime.TabIndex = 1;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.Cyan;
            this.label20.Location = new System.Drawing.Point(628, 331);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(189, 29);
            this.label20.TabIndex = 0;
            this.label20.Text = "Sampling Time :";
            // 
            // mointer_refresh_timer
            // 
            this.mointer_refresh_timer.Tick += new System.EventHandler(this.mointer_refresh_timer_Tick);
            // 
            // lbTMState
            // 
            this.lbTMState.AutoSize = true;
            this.lbTMState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lbTMState.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lbTMState.Location = new System.Drawing.Point(1033, 699);
            this.lbTMState.Name = "lbTMState";
            this.lbTMState.Size = new System.Drawing.Size(130, 25);
            this.lbTMState.TabIndex = 8;
            this.lbTMState.Text = "Torque Meter";
            // 
            // lbPAState
            // 
            this.lbPAState.AutoSize = true;
            this.lbPAState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lbPAState.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lbPAState.Location = new System.Drawing.Point(1033, 736);
            this.lbPAState.Name = "lbPAState";
            this.lbPAState.Size = new System.Drawing.Size(149, 25);
            this.lbPAState.TabIndex = 9;
            this.lbPAState.Text = "Power Analyzer";
            // 
            // lbSDState
            // 
            this.lbSDState.AutoSize = true;
            this.lbSDState.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lbSDState.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.lbSDState.Location = new System.Drawing.Point(1033, 774);
            this.lbSDState.Name = "lbSDState";
            this.lbSDState.Size = new System.Drawing.Size(114, 25);
            this.lbSDState.TabIndex = 10;
            this.lbSDState.Text = "Servo Drive";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(1029, 669);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(110, 18);
            this.label22.TabIndex = 11;
            this.label22.Text = "Connect State :";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1199, 845);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.lbSDState);
            this.Controls.Add(this.lbPAState);
            this.Controls.Add(this.lbTMState);
            this.Controls.Add(this.btnAutoControl);
            this.Controls.Add(this.btnManualControl);
            this.Controls.Add(this.btnCOMM);
            this.Controls.Add(this.monitor_gb);
            this.Controls.Add(this.gbCOMM);
            this.Controls.Add(this.gbAutoControl);
            this.Controls.Add(this.gbManualControl);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.Text = "Dyno";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.monitor_gb.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.gbCOMM.ResumeLayout(false);
            this.gbCOMM.PerformLayout();
            this.gbManualControl.ResumeLayout(false);
            this.gbManualControl.PerformLayout();
            this.gbGenMode.ResumeLayout(false);
            this.gbGenMode.PerformLayout();
            this.gbMotorMode.ResumeLayout(false);
            this.gbMotorMode.PerformLayout();
            this.gbAutoControl.ResumeLayout(false);
            this.gbAutoControl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox monitor_gb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCOMM;
        private System.Windows.Forms.Button btnManualControl;
        private System.Windows.Forms.Button btnAutoControl;
        private System.Windows.Forms.GroupBox gbCOMM;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox manual_set_speed_tb;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox gbManualControl;
        private System.Windows.Forms.GroupBox gbAutoControl;
        private System.Windows.Forms.ComboBox cbTMComportList;
        private System.Windows.Forms.Button btnTMConnect;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btnSDconnect;
        private System.Windows.Forms.ComboBox cbSDComportList;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnPAconnect;
        private System.Windows.Forms.ComboBox cbPAComportList;
        private System.Windows.Forms.GroupBox gbGenMode;
        private System.Windows.Forms.GroupBox gbMotorMode;
        private System.Windows.Forms.Button btnSaveFile;
        private System.Windows.Forms.Button btnCatchData;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox manual_set_torque_tb;
        private System.Windows.Forms.RadioButton rbGenModeSeleted;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.RadioButton rbMotorModeSeleted;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button manual_start_btn;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tbSampleTime;
        private System.Windows.Forms.RichTextBox rtbManualLOG;
        private System.Windows.Forms.Button btnAutoStart;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnAutoStop;
        private System.Windows.Forms.Button manual_stop_btn;
        private System.Windows.Forms.Label Vdc_value;
        private System.Windows.Forms.Label InputPower_value;
        private System.Windows.Forms.Label Idc_value;
        private System.Windows.Forms.Label Torque_value;
        private System.Windows.Forms.Label DrivePower_value;
        private System.Windows.Forms.Label Iw_value;
        private System.Windows.Forms.Label Vvw_value;
        private System.Windows.Forms.Label Iu_value;
        private System.Windows.Forms.Label Vuv_value;
        private System.Windows.Forms.Label MotorEff_value;
        private System.Windows.Forms.Label DriveEff_value;
        private System.Windows.Forms.Label MotorPower_value;
        private System.Windows.Forms.Label Speed_value;
        private System.Windows.Forms.Timer mointer_refresh_timer;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label Motor_pwr_unit;
        private System.Windows.Forms.Label speed_unit;
        private System.Windows.Forms.Label torque_unit;
        private System.Windows.Forms.Label Drive_pwr_unit;
        private System.Windows.Forms.Label Iw_unit;
        private System.Windows.Forms.Label Vvw_unit;
        private System.Windows.Forms.Label Iu_unit;
        private System.Windows.Forms.Label Vuv_unit;
        private System.Windows.Forms.Label input_pwr_unit;
        private System.Windows.Forms.Label Idc_unit;
        private System.Windows.Forms.Label Vdc_unit;
        private System.Windows.Forms.Label Motor_eff_unit;
        private System.Windows.Forms.Label Drive_eff_unit;
        private System.Windows.Forms.Label lbTMState;
        private System.Windows.Forms.Label lbPAState;
        private System.Windows.Forms.Label lbSDState;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button btnSetSP;
        private System.Windows.Forms.Label lbTQunit;
        private System.Windows.Forms.Button btnSetTQ;
        private System.Windows.Forms.Label lbSPunit;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button btnClearAll;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
    }
}

