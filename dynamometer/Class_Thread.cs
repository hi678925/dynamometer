﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace dynamometer
{
    class Class_Thread
    {
        private Thread thread = null;
        public ThreadState thread_state = ThreadState.Stopped;

        public Class_Thread()
        {

        }

        public delegate void Process();
        private Process _Process;
        private string _thread_name = "";
        private ThreadPriority _thread_priority;

        public void initial(string thread_name, ThreadPriority thread_priority, Process process, bool is_background = true)
        {
            _thread_name = thread_name;
            _thread_priority = thread_priority;
            _Process = process;

            thread = new Thread(new ThreadStart(_Process));
            thread.Name = _thread_name;
            thread.Priority = _thread_priority;
            thread.IsBackground = is_background;
        }

        public void start()
        {
            if (thread == null)
            {
                thread = new Thread(new ThreadStart(_Process));
                thread.Name = _thread_name;
                thread.Priority = _thread_priority;
                thread.IsBackground = true;
            }

            if (thread_state == ThreadState.Unstarted || thread_state == ThreadState.Stopped)
            {
                thread.Start();
                thread_state = ThreadState.Running;
            }

            if (thread_state == ThreadState.Suspended)
            {
                thread_state = ThreadState.Running;
            }
            Thread.Sleep(100);
        }

        public void pause()
        {
            if (thread_state != ThreadState.SuspendRequested)
            {
                thread_state = ThreadState.SuspendRequested;
                Thread.Sleep(100);
                while (thread_state != ThreadState.Suspended)
                {
                    Thread.Sleep(10);
                }
            }
            Thread.Sleep(1);
        }

        public void stop()
        {
            if (thread_state != ThreadState.Stopped && thread_state != ThreadState.Unstarted)
            {
                thread_state = ThreadState.StopRequested;
                Thread.Sleep(100);
                while (thread_state != ThreadState.Stopped)
                {
                    Thread.Sleep(10);
                }
            }

            thread = null;
            Thread.Sleep(1);
        }

        public ThreadState moniter()
        {
            // 請求暫停
            if (thread_state == ThreadState.SuspendRequested)
            {
                thread_state = ThreadState.Suspended;
                return thread_state;
            }

            // 已暫停
            if (thread_state == ThreadState.Suspended)
            {
                Thread.Sleep(100);
                return thread_state;
            }

            // 請求停止
            if (thread_state == ThreadState.StopRequested)
            {
                thread_state = System.Threading.ThreadState.Stopped;
                return thread_state;
            }

            // 已停止
            if (thread_state == ThreadState.Stopped)
            {
                Thread.Sleep(100);
                return thread_state;
            }

            return thread_state;
        }
    }
}
