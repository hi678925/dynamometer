﻿using System;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace dynamometer
{
    public partial class MainForm : Form
    {
        public void TM_Send_Handler()
        {
            byte[] cmdTMReadData = { 0x01, 0x03, 0x00, 0x00, 0x00, 0x06, 0xC5, 0xC8 };
            while (true)
            {
                c_Thread_TM_Send.thread_state = c_Thread_TM_Send.moniter();
                if (c_Thread_TM_Send.thread_state == ThreadState.Running)
                {
                    try
                    {
                        c_SerialPort_TM.SendData(cmdTMReadData);
                    }
                    catch (Exception ex)
                    {
                        // exception handler 
                        // 看要怎麼處理
                        c_SerialPort_TM._serialPort.Close();
                        MessageBox.Show(String.Format("Error:{0}", ex.ToString()));
                        Thread.Sleep(3000);
                    }
                }
                else if (c_Thread_TM_Send.thread_state == ThreadState.SuspendRequested ||
                        c_Thread_TM_Send.thread_state == ThreadState.Suspended ||
                        c_Thread_TM_Send.thread_state == ThreadState.StopRequested)
                {
                    Thread.Sleep(500);
                }
                else if (c_Thread_TM_Send.thread_state == ThreadState.Stopped)
                {
                    break;
                }
                Thread.Sleep(200);
            }
        }
        public void TM_Receive_Handler()
        {
            int buf_len = 0;
            byte[] receive_buf;
            byte[] receive_buf_CRC;
            int torque_data_value, speed_data;
            double torque_data, motor_power_data;
            //byte[] data_example = {0x01, 0x03, 0x0C, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x87, 0x75};

            while (true)
            {
                c_Thread_TM_Receive.thread_state = c_Thread_TM_Receive.moniter();
                if (c_Thread_TM_Receive.thread_state == ThreadState.Running)
                {
                    try
                    {
                        buf_len = c_SerialPort_TM._serialPort.BytesToRead;
                        if (buf_len > 0)
                        {
                            receive_buf = c_SerialPort_TM.ReadData();
                            receive_buf_CRC = Class_DataProcessing.CalcCRC(receive_buf, (receive_buf.Length-2));

                            if (Class_DataProcessing.CheckCRC(receive_buf, receive_buf_CRC))   //check CRC
                            {
                                Array.Reverse(receive_buf, 3, 12);  // Little endian

                                torque_data_value = BitConverter.ToInt32(receive_buf, 11);
                                speed_data = BitConverter.ToInt32(receive_buf, 7);
                                //motor_power_data = BitConverter.ToInt32(receive_buf, 3); //Resolution is not enough.
                                torque_data = Convert.ToDouble(torque_data_value) / 100;      
                                motor_power_data = (torque_data * speed_data) / 30 * Math.PI;                                

                                s_Mointer.motor_torque = (torque_data);
                                s_Mointer.motor_speed = (Convert.ToDouble(speed_data));
                                s_Mointer.motor_pwr = Math.Round(motor_power_data, 2);
                            }
                        }
                    }
                    catch //(Exception ex)
                    {
                        // exception handler 
                        // 看要怎麼處理
                        //MessageBox.Show(ex.Message);
                        //Thread.Sleep(3000);
                    }
                }
                else if (c_Thread_TM_Receive.thread_state == ThreadState.SuspendRequested ||
                        c_Thread_TM_Receive.thread_state == ThreadState.Suspended ||
                        c_Thread_TM_Receive.thread_state == ThreadState.StopRequested)
                {
                    Thread.Sleep(500);
                }
                else if (c_Thread_TM_Receive.thread_state == ThreadState.Stopped)
                {
                    break;
                }
                Thread.Sleep(50);
            }
        }
        public void PA_Init_task()
        {
            int delay = 200;  // delay 200ms

            c_SerialPort_PA.SendData(cmdPAInit);
            Thread.Sleep(delay);
            c_SerialPort_PA.SendData(cmdPASetVolt1);
            Thread.Sleep(delay);
            c_SerialPort_PA.SendData(cmdPASetVolt2);
            Thread.Sleep(delay);
            c_SerialPort_PA.SendData(cmdPASetVolt3);
            Thread.Sleep(delay);
            c_SerialPort_PA.SendData(cmdPASetCurr1);
            Thread.Sleep(delay);
            c_SerialPort_PA.SendData(cmdPASetCurr2);
            Thread.Sleep(delay);
            c_SerialPort_PA.SendData(cmdPASetCurr3);
            Thread.Sleep(delay);
            c_SerialPort_PA.SendData(cmdPASetFreq1);
            Thread.Sleep(delay);
            c_SerialPort_PA.SendData(cmdPASetFreq2);
            Thread.Sleep(delay);
            c_Thread_PA_Send.start();
            c_Thread_PA_Receive.start();
            PAInitTaskFinishFlag = true;
        }
        public void Thread_AutoControl_task(int stepNums = 0)
        {
            for(int i = 0; i <= stepNums; i++)
            {
                if (AutoStepParamList[i].unit == "rpm")     // Speed mode
                {
                    int curveTime, smoothTime;

                    c_MotorControl_SD.SetTorquePercent(200);
                    SetSDSpeed(AutoStepParamList[i].value);
                    curveTime = Convert.ToInt16(Convert.ToDouble(AutoStepParamList[i].stepTime) * 9 / 10 * 1000);  // msec -> sec
                    smoothTime = Convert.ToInt16(Convert.ToDouble(AutoStepParamList[i].stepTime) * 1 / 10 * 1000); // msec -> sec
                    c_MotorControl_SD.SetScurve(curveTime, curveTime, smoothTime);
                    c_MotorControl_SD.Run();
                    
                    System.Diagnostics.Debug.WriteLine("Speed= {0} rpm, stepTime = {1}, stableTime = {2}, Time = {3}", AutoStepParamList[i].value, AutoStepParamList[i].stepTime, AutoStepParamList[i].stableTime, DateTime.Now.ToString("HH:mm:ss"));
                }
                else if (AutoStepParamList[i].unit == "N.m") // Torque mode
                {
                    if((i == 0) || (AutoStepParamList[i - 1].unit == "N.m"))
                    {
                        int timesToRaise;
                        double slope;
                        double tqCmd = 0;
                        
                        c_MotorControl_SD.SetSpeed(0);
                        
                        timesToRaise = Convert.ToInt16(Convert.ToDouble(AutoStepParamList[i].stepTime) / 0.2);  // ex.stepTime = 2 sec, timesToRaise = 10 times.
                        if (i != 0){ 
                            tqCmd = Convert.ToDouble(AutoStepParamList[i-1].value);
                        }
                        slope = (Convert.ToDouble(AutoStepParamList[i].value) - tqCmd) / timesToRaise;

                        for (int counter = 0; counter < timesToRaise; counter++){
                            tqCmd += slope;
                            SetSDTorqueNm(tqCmd);  
                            System.Diagnostics.Debug.WriteLine("tqCmd= {0} N.m, Time = {1}", tqCmd, DateTime.Now.ToString("HH:mm:ss"));
                            Thread.Sleep(200);
                        }
                    }
                    else if(AutoStepParamList[i-1].unit == "rpm")
                    {
                        int timesToFall;
                        int slope;
                        int spCmd;

                        spCmd = Convert.ToInt16(AutoStepParamList[i - 1].value);
                        timesToFall = Convert.ToInt16(Convert.ToDouble(AutoStepParamList[i].stepTime) / 0.2);
                        slope = Convert.ToInt16(Math.Ceiling(Convert.ToDouble(AutoStepParamList[i - 1].value) / timesToFall));
                        for (int counter = 0; counter < timesToFall; counter++)
                        {
                            spCmd -= slope;
                            if(spCmd <= 0)
                            {
                                spCmd = 0;
                            }
                            SetSDSpeed(spCmd);
                            System.Diagnostics.Debug.WriteLine("spCmd= {0} rpm, Time = {1}", spCmd, DateTime.Now.ToString("HH:mm:ss"));
                            Thread.Sleep(200);
                        }
                        SetSDTorqueNm(AutoStepParamList[i].value);
                        System.Diagnostics.Debug.WriteLine("tqCmd= {0} N.m, Time = {1}", AutoStepParamList[i].value, DateTime.Now.ToString("HH:mm:ss"));
                    }   
                    //System.Diagnostics.Debug.WriteLine("Torque= {0} N.m, stepTime = {1}, stableTime = {2}", AutoStepParamList[i].value, AutoStepParamList[i].stepTime, AutoStepParamList[i].stableTime);
                }
                Thread.Sleep(Int32.Parse(AutoStepParamList[i].stableTime) * 1000);
            }
        }

        public void PA_Send_Handler()
        {
            string cmdPAReadData = ":MEAS? U3,I3,U1,I1,U2,I2,P3,P0, EFF2";
            while (true)
            {
                c_Thread_PA_Send.thread_state = c_Thread_PA_Send.moniter();
                if (c_Thread_PA_Send.thread_state == ThreadState.Running)
                {
                    try
                    {                       
                        c_SerialPort_PA._serialPort.WriteLine(cmdPAReadData);
                    }
                    catch //(Exception ex)
                    {
                        // exception handler 
                        // 看要怎麼處理
                        //MessageBox.Show(ex.Message);
                        //Thread.Sleep(3000);
                    }
                }
                else if (c_Thread_PA_Send.thread_state == ThreadState.SuspendRequested ||
                        c_Thread_PA_Send.thread_state == ThreadState.Suspended ||
                        c_Thread_PA_Send.thread_state == ThreadState.StopRequested)
                {
                    Thread.Sleep(500);
                }
                else if (c_Thread_PA_Send.thread_state == ThreadState.Stopped)
                {
                    break;
                }
                Thread.Sleep(200);
            }
        }
        public void PA_Receive_Handler()
        {
            int buf_len = 0;
            double motor_eff;
            byte[] receive_buf = new byte[129];
            //String data_example = "U3 +00.000E+0;I3 +00.000E+0;U1 +00.000E+0;I1 +00.000E+0;U2 +00.000E+0;I2 +00.000E+0;P3 +0.0000E+3;P0 +0.0000E+3;EFF2 +999.99E+9";
            while (true)
            {
                c_Thread_PA_Receive.thread_state = c_Thread_PA_Receive.moniter();
                if (c_Thread_PA_Receive.thread_state == ThreadState.Running)
                {
                    try
                    {
                        buf_len = c_SerialPort_PA._serialPort.BytesToRead;
                        if (buf_len > 129)
                        {
                            //Thread.Sleep(100);
                            //Console.WriteLine(c_SerialPort_PA._serialPort.BytesToRead.ToString());
                            //receive_buf_str = c_SerialPort_PA.ReadExistingData();
                            receive_buf = c_SerialPort_PA.ReadData();
                            pa_receive_buf_str = Encoding.Default.GetString(receive_buf);
                            pa_receive_buf_str_split = pa_receive_buf_str.Split(';');
                            foreach (var str_split in pa_receive_buf_str_split)
                            {
                                string header = str_split.Split(' ')[0];
                                string data = str_split.Split(' ')[1];
                                switch (header)
                                {
                                    case "U1":
                                        s_Mointer.output_vuv = Convert.ToDouble(data);
                                        break;
                                    case "I1":
                                        s_Mointer.output_iu = Convert.ToDouble(data);
                                        break;
                                    case "U2":
                                        s_Mointer.output_vvw = Convert.ToDouble(data);
                                        break;
                                    case "I2":
                                        s_Mointer.output_iw = Convert.ToDouble(data);
                                        break;
                                    case "U3":
                                        s_Mointer.input_vdc = Convert.ToDouble(data);
                                        break;
                                    case "I3":
                                        s_Mointer.input_idc = Convert.ToDouble(data);
                                        break;
                                    case "P3":
                                        s_Mointer.input_pwr = Convert.ToDouble(data);
                                        break;
                                    case "P0":
                                        s_Mointer.output_drive_pwr = Convert.ToDouble(data);
                                        break;
                                    case "EFF2":
                                        s_Mointer.efficency_drive_eff = Convert.ToDouble(data);
                                        break;
                                }

                                if ((s_Mointer.motor_pwr != 0) && (s_Mointer.output_drive_pwr != 0))
                                {
                                    motor_eff = (s_Mointer.motor_pwr / s_Mointer.output_drive_pwr);
                                    s_Mointer.efficency_motor_eff = Math.Round(motor_eff, 2);
                                }
                            }

                        }
                    }
                    catch //(Exception ex)
                    {
                        // exception handler 
                        // 看要怎麼處理
                        //MessageBox.Show(ex.Message);
                        //Thread.Sleep(3000);
                    }
                }
                else if (c_Thread_PA_Receive.thread_state == ThreadState.SuspendRequested ||
                        c_Thread_PA_Receive.thread_state == ThreadState.Suspended ||
                        c_Thread_PA_Receive.thread_state == ThreadState.StopRequested)
                {
                    Thread.Sleep(1);  //500
                }
                else if (c_Thread_PA_Receive.thread_state == ThreadState.Stopped)
                {
                    break;
                }
                Thread.Sleep(200);
            }
        }
        public void Send_LOG_Handler()
        {
            string AutoLOG;
            while (true)
            {
                c_Thread_send_LOG.thread_state = c_Thread_send_LOG.moniter();
                if (c_Thread_send_LOG.thread_state == ThreadState.Running)
                {
                    try
                    {
                        PlotForm plotform = new PlotForm();
                        AutoLOG = string.Format("{0:00.00}", s_Mointer.input_vdc) + ", " + string.Format("{0:00.00}", s_Mointer.input_idc) + ", " +
                        string.Format("{0:0000.00}", s_Mointer.input_pwr) + ", " +

                        string.Format("{0:00.00}", s_Mointer.output_vuv) + ", " + string.Format("{0:00.00}", s_Mointer.output_vvw) + ", " +
                        string.Format("{0:00.00}", s_Mointer.output_iu) + ", " + string.Format("{0:00.00}", s_Mointer.output_iw) + ", " +
                        string.Format("{0:0000.00}", s_Mointer.output_drive_pwr) + ", " +

                        string.Format("{0:00.00}", s_Mointer.motor_torque) + ", " + string.Format("{0:000.0}", s_Mointer.motor_speed) + ", " +
                        string.Format("{0:0000.00}", s_Mointer.motor_pwr) + ", " +

                        string.Format("{0:00.00}", s_Mointer.efficency_drive_eff) + ", " + string.Format("{0:00.00}", s_Mointer.efficency_motor_eff) + ", "
                         + Environment.NewLine;
                        plotform.UpdateRichTextBox(AutoLOG);
                    }
                    catch //(Exception ex)
                    {
                        // exc/eption handler 
                        // 看要怎麼處理
                        //MessageBox.Show(ex.Message);
                        //Thread.Sleep(3000);
                    }
                }
                else if (c_Thread_send_LOG.thread_state == ThreadState.SuspendRequested ||
                        c_Thread_send_LOG.thread_state == ThreadState.Suspended ||
                        c_Thread_send_LOG.thread_state == ThreadState.StopRequested)
                {
                    Thread.Sleep(500);
                }
                else if (c_Thread_send_LOG.thread_state == ThreadState.Stopped)
                {
                    break;
                }
                Thread.Sleep((int)samplingTime);
            }
        }
        public void SD_Receive_Handler()
        {
            int buf_len = 0;
            byte[] receive_buf;

            while (true)
            {
                c_Thread_SD_Receive.thread_state = c_Thread_SD_Receive.moniter();
                if (c_Thread_SD_Receive.thread_state == ThreadState.Running)
                {
                    try
                    {
                        buf_len = c_SerialPort_SD._serialPort.BytesToRead;
                        if (buf_len > 0)
                        {
                            receive_buf = c_SerialPort_SD.ReadData();
                        }
                    }
                    catch //(Exception ex)
                    {
                        // exception handler 
                        // 看要怎麼處理
                        //MessageBox.Show(ex.Message);
                        //Thread.Sleep(3000);
                    }
                }
                else if (c_Thread_SD_Receive.thread_state == ThreadState.SuspendRequested ||
                        c_Thread_SD_Receive.thread_state == ThreadState.Suspended ||
                        c_Thread_SD_Receive.thread_state == ThreadState.StopRequested)
                {
                    Thread.Sleep(500);
                }
                else if (c_Thread_SD_Receive.thread_state == ThreadState.Stopped)
                {
                    break;
                }
                Thread.Sleep(50);
            }
        }
    }
}
