﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace dynamometer
{
    public partial class PlotForm : Form
    {
        static string _tmpLogFromForm1;
        public PlotForm()
        {
            InitializeComponent();

            this.StartPosition = FormStartPosition.Manual;
            this.Location = new Point(750, 50);
            this.Size = new Size(800, 450);   // 971

            

            /*this.chart1.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            this.chart1.Series[0].Points.AddXY(32.0, 50.0);
            this.chart1.Series[0].Points.AddXY(33.5, 40.0);
            this.chart1.Series[0].Points.AddXY(34.0, 20.0);

            this.chart1.Series[1].Points.AddXY(32.0, 20.0);
            this.chart1.Series[1].Points.AddXY(33.5, 40.0);
            this.chart1.Series[1].Points.AddXY(34.0, 50.0);*/
        }
        
        private void PlotForm_Load(object sender, EventArgs e)
        {
        }
        public void UpdateRichTextBox(string strLOG) //System.Windows.Forms.RichTextBox rtbAutoLOG, string strLOG)
        {
            _tmpLogFromForm1 = strLOG;
        }
        private void SaveCsvFile(string dataText)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            //設定檔案型別 
            sfd.Filter = "Csv檔案(*.csv)|*.csv|文字檔案(*.txt)|*.txt|所有檔案(*.*)|*.*";//設定檔案型別
            sfd.FileName = "Data" + DateTime.Now.ToString("yyyy-MM-dd HHmmss");//設定預設檔名
            sfd.DefaultExt = "csv";//設定預設格式（可以不設） //"txt";
            sfd.AddExtension = true;//設定自動在檔名中新增副檔名            
            sfd.RestoreDirectory = true;//儲存對話方塊是否記憶上次開啟的目錄 

            //點了儲存按鈕進入 
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter swFileText = new StreamWriter(sfd.FileName))
                {
                    swFileText.WriteLine("input_vdc,input_idc,input_pwr,output_vuv,output_vvw,output_iu,output_iw,output_drive_pwr,motor_torque,motor_speed,motor_pwr,efficency_drive_eff,efficency_motor_eff");
                    swFileText.Write(dataText);
                }
            }
        }

        private void btnClearAll_Click(object sender, EventArgs e)
        {
            rtbAutoLOG.Clear();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (_tmpLogFromForm1 != null && _tmpLogFromForm1 != "")
            {
                this.rtbAutoLOG.AppendText(_tmpLogFromForm1);
                _tmpLogFromForm1 = "";
            }
        }

        private void btnSaveFile_Click(object sender, EventArgs e)
        {
            SaveCsvFile(this.rtbAutoLOG.Text);
        }
    }
}
