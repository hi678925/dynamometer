﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO.Ports;
using System.Management;
using System.IO;

namespace dynamometer
{
    public partial class MainForm : Form
    {

        private bool button_connect_status = false;
        private bool PA_button_connect_status = false;
        private bool SD_button_connect_status = false;

        public MainForm()
        {
            InitializeComponent();

            // Initialize
            Form form1 = new Form();
            form1.Size = new Size(840, 720);

            gbCOMM.Left = 9;
            gbCOMM.Top = 272;
            gbCOMM.Width = 674;
            gbCOMM.Height = 400;
            gbCOMM.Visible = true;          //comm is the first page.
            gbManualControl.Visible = false;
            gbAutoControl.Visible = false;

            gbMotorMode.BackColor = Color.Black;
            gbMotorMode.ForeColor = Color.Lime;

            gbGenMode.BackColor = Color.Black;
            gbGenMode.ForeColor = Color.Cyan;

            lbTMState.BackColor = Color.FromArgb(255, 192, 192);  // Red
            lbPAState.BackColor = Color.FromArgb(255, 192, 192);  // Red
            lbSDState.BackColor = Color.FromArgb(255, 192, 192);  // Red

            Motor_pwr_unit.BackColor = System.Drawing.Color.Transparent; 
            input_pwr_unit.BackColor= System.Drawing.Color.Transparent;

            rbMotorModeSeleted.Checked = true;
            gbMotorMode.Enabled = true;
            gbGenMode.Enabled = false;

            manual_start_btn.Enabled = true;
            manual_stop_btn.Enabled = false;

            btnAutoStart.Enabled = true;
            btnAutoStop.Enabled = false;

            mointer_refresh_timer.Interval = 200;  // 200 ms

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            InitThread();
            InitUIElement();
            InitVariable();
            mointer_refresh_timer.Enabled = true;
        }

        private void InitThread()
        {
            c_Thread_TM_Send.initial("TM_Thread_Send", ThreadPriority.Normal, TM_Send_Handler);
            c_Thread_PA_Send.initial("PA_Thread_Send", ThreadPriority.Normal, PA_Send_Handler);
            

            c_Thread_TM_Receive.initial("TM_Thread_Receive", ThreadPriority.Normal, TM_Receive_Handler);
            c_Thread_PA_Receive.initial("PA_Thread_Receive", ThreadPriority.Normal, PA_Receive_Handler);
            c_Thread_SD_Receive.initial("SD_Thread_Receive", ThreadPriority.Normal, SD_Receive_Handler);

            c_Thread_send_LOG.initial("c_Thread_send_LOG", ThreadPriority.Normal, Send_LOG_Handler);
        }

        private void InitUIElement()
        {
            this.StartPosition = FormStartPosition.Manual;
            this.Location = new Point(50, 50);
            this.Size = new Size(915, 730);

            gbCOMM.Size = new Size(755, 400);
            gbCOMM.Left = 9;
            gbCOMM.Top = 280;

            gbAutoControl.Size = new Size(755, 400);
            gbAutoControl.Left = 9;
            gbAutoControl.Top = 280;

            gbManualControl.Size = new Size(755, 400);
            gbManualControl.Left = 9;
            gbManualControl.Top = 280;

            monitor_gb.Size = new Size(875, 270);
            monitor_gb.Left = 9;
            monitor_gb.Top = 10;

            btnCOMM.Left = 775;
            btnCOMM.Top = 290;

            btnManualControl.Left = 775;
            btnManualControl.Top = 335;

            btnAutoControl.Left = 775;
            btnAutoControl.Top = 380;
            
            for (int i = 0; i < 20; i++)
            {
                checkBox1 = new CheckBox();
                this.checkBox1.Name = "chk_" + (i + 1).ToString();
                this.checkBox1.Text = (i+1).ToString();
                this.checkBox1.Size = new Size(60, 38);
                this.checkBox1.Location = new Point(0, i * 35);
                this.panel1.Controls.Add(checkBox1);
            }
            for (int i = 0; i < 60; i++)
            {
                int itemX;
                int itemY;
                itemX = i % 3;
                itemY = i / 3;

                textBox1 = new TextBox();
                this.textBox1.Name = "tb_" + (i + 1).ToString();
                //this.textBox1.Text = (i + 1).ToString(); // for test
                this.textBox1.Size = new Size(70, 34);

                if (itemX == 0)
                {
                    this.textBox1.Location = new Point(itemX * 120 + 60, itemY * 35);
                }
                else
                    this.textBox1.Location = new Point(itemX * 100 + 120, itemY * 35);

                this.panel1.Controls.Add(textBox1);
            }
            for (int i = 0; i < 20; i++)
            {
                comboBox1 = new ComboBox();
                this.comboBox1.Name = "cbo_" + (i + 1).ToString();
                this.comboBox1.Font = new Font("Arial", 10, FontStyle.Regular);              
                this.comboBox1.Size = new Size(60, 38);
                this.comboBox1.Location = new Point(140, i * 35 + 2);
                this.panel1.Controls.Add(comboBox1);           
                this.comboBox1.Items.Add("rpm");
                this.comboBox1.Items.Add("N.m");
                this.comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
                this.comboBox1.SelectedIndex = 0;    // init = rpm
            }
        }

        private void InitVariable()
        {
            s_Mointer.input_vdc = 0;
            s_Mointer.input_idc = 0;
            s_Mointer.input_pwr = 0;

            s_Mointer.output_vuv = 0;
            s_Mointer.output_vvw = 0;
            s_Mointer.output_iu = 0;
            s_Mointer.output_iw = 0;
            s_Mointer.output_drive_pwr = 0;

            s_Mointer.motor_torque = 0;
            s_Mointer.motor_speed = 0;
            s_Mointer.motor_pwr = 0;

            s_Mointer.efficency_drive_eff = 0;
            s_Mointer.efficency_motor_eff = 0;      
        }

        private void PowerAnalyzer_Init_cmd()
        {
            Thread_PA_Init = new Thread(PA_Init_task);
            Thread_PA_Init.IsBackground = true;
            Thread_PA_Init.Start();
        }

        private void AutoControlCmd(int stepNum)
        {
            Thread_AutoControl = new Thread(() => Thread_AutoControl_task(stepNum));
            Thread_AutoControl.IsBackground = true;
            Thread_AutoControl.Start();
        }

        private void ServoDriver_Init_cmd()
        {
            c_SerialPort_SD.SendData(cmdSDInit);
            Thread.Sleep(100);
            c_SerialPort_SD.SendData(cmdSDUnsaved);
        }

        private void StringToByte_Send(string data_s)
        {
            byte[] data_b = Encoding.ASCII.GetBytes(data_s);
            c_SerialPort_PA._serialPort.Write(data_b + "\r\n");
        }

        private string GetTbValue(int i)
        {
            string controlName;
            controlName = "tb_" + i.ToString();
            Control[] ctl = this.Controls.Find(controlName, true);
            return ((TextBox)ctl[0]).Text;
        }
        private string GetCboValue(int i)
        {
            ComboBox _cbo = this.Controls.Find("cbo_" + i.ToString(), true).FirstOrDefault() as ComboBox;
            return _cbo.Text;
        }
        private void SetSDSpeed(string strSpeed)
        {
            SetSDSpeed(Convert.ToInt32(strSpeed));
        }
        private void SetSDSpeed(int Speed)
        {
            int speedCMD;
            int ReductionRatio = 10;
            int DeltaCmdRatio = 10;  

            speedCMD = Speed * ReductionRatio * DeltaCmdRatio;               
            c_MotorControl_SD.SetSpeed(speedCMD);         
        }
        private void SetSDTorqueNm(string strTorqueNm)
        {
            SetSDTorqueNm(Convert.ToDouble(strTorqueNm));
        }
        private void SetSDTorqueNm(double douTorqueNm)
        {
            double RatedTorque = 2.39;
            double ReductionRatio = 10;
            int torque_percent;

            torque_percent = Convert.ToInt32(Math.Round(douTorqueNm/ ReductionRatio/ RatedTorque * 100));
            if (torque_percent >= 300)  // Max : 300%
            {
                torque_percent = 300;
                manual_set_torque_tb.Text = Convert.ToString(torque_percent * RatedTorque * ReductionRatio / 100);
            }
            else if (torque_percent <= -300)  // Min : -300%
            {
                torque_percent = -300;
                manual_set_torque_tb.Text = Convert.ToString(torque_percent * RatedTorque * ReductionRatio / 100);
            }        
            c_MotorControl_SD.SetTorquePercent(torque_percent);
        }

        private void SaveCsvFile(string dataText)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            //設定檔案型別 
            sfd.Filter = "Csv檔案(*.csv)|*.csv|文字檔案(*.txt)|*.txt|所有檔案(*.*)|*.*";//設定檔案型別
            sfd.FileName = "Data" + DateTime.Now.ToString("yyyy-MM-dd HHmmss");//設定預設檔名
            sfd.DefaultExt = "csv";//設定預設格式（可以不設） //"txt";
            sfd.AddExtension = true;//設定自動在檔名中新增副檔名            
            sfd.RestoreDirectory = true;//儲存對話方塊是否記憶上次開啟的目錄 

            //點了儲存按鈕進入 
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter swFileText = new StreamWriter(sfd.FileName))
                {
                    swFileText.WriteLine("input_vdc,input_idc,input_pwr,output_vuv,output_vvw,output_iu,output_iw,output_drive_pwr,motor_torque,motor_speed,motor_pwr,efficency_drive_eff,efficency_motor_eff");
                    swFileText.Write(dataText);
                }
            }
        }

        private void cbTMComportList_DropDown(object sender, EventArgs e)
        {
            cbTMComportList.Items.Clear();
            foreach (string com in System.IO.Ports.SerialPort.GetPortNames())//取得所有可用的連接埠
            {
                cbTMComportList.Items.Add(com);
            }
        }

        private void btnTMConnect_Click(object sender, EventArgs e)
        {           
            if (button_connect_status)
            {
                // Close com port
                c_Thread_TM_Send.stop();
                c_Thread_TM_Receive.stop();

                if (c_SerialPort_TM.CloseComPort())
                {
                    cbTMComportList.Enabled = true;
                    btnTMConnect.Text = "Connect";
                    button_connect_status = false;
                    lbTMState.BackColor = Color.FromArgb(255, 192, 192);  // Red
                }
            }
            else
            {
                if (!c_SerialPort_TM.sending && c_SerialPort_TM.OpenComPort(cbTMComportList.Text, tm_buadrate))
                {
                    cbTMComportList.Enabled = false;
                    btnTMConnect.Text = "Disconnect";
                    button_connect_status = true;
                    lbTMState.BackColor = Color.FromArgb(192, 255, 192);  // Green

                    c_Thread_TM_Send.start();
                    c_Thread_TM_Receive.start();
                }
            }
        }

        private void cbPAComportList_DropDown(object sender, EventArgs e)
        {
            cbPAComportList.Items.Clear();
            foreach (string com in System.IO.Ports.SerialPort.GetPortNames())//取得所有可用的連接埠
            {
                cbPAComportList.Items.Add(com);
            }
        }

        private void btnPAconnect_Click(object sender, EventArgs e)
        {
            if (PA_button_connect_status)
            {
                c_Thread_PA_Send.stop();
                c_Thread_PA_Receive.stop();
                // Close com port
                if (c_SerialPort_PA.CloseComPort())
                {
                    cbPAComportList.Enabled = true;
                    btnPAconnect.Text = "Connect";
                    PA_button_connect_status = false;
                    lbPAState.BackColor = Color.FromArgb(255, 192, 192);  // Red
                    PAInitTaskFinishFlag = true;
                }
            }
            else
            {
                // Open com port
                if (c_SerialPort_PA.OpenComPort(cbPAComportList.Text, pa_buadrate))
                {
                    cbPAComportList.Enabled = false;
                    btnPAconnect.Text = "Disconnect";
                    PA_button_connect_status = true;
                    lbPAState.BackColor = Color.FromArgb(192, 255, 192);  // Green
                    PAInitTaskFinishFlag = false;
                    PowerAnalyzer_Init_cmd();  // init
                }

            }
        }

        private void cbSDComportList_DropDown(object sender, EventArgs e)
        {
            cbSDComportList.Items.Clear();
            foreach (string com in System.IO.Ports.SerialPort.GetPortNames())//取得所有可用的連接埠
            {
                cbSDComportList.Items.Add(com);
            }
        }

        private void btnSDconnect_Click(object sender, EventArgs e)
        {
            if (SD_button_connect_status)
            {
                //c_Thread_SD_Send.stop();
                //c_Thread_SD_Receive.stop();
                // Close com port
                if (c_SerialPort_SD.CloseComPort())
                {
                    cbSDComportList.Enabled = true;
                    btnSDconnect.Text = "Connect";
                    SD_button_connect_status = false;
                    lbSDState.BackColor = Color.FromArgb(255, 192, 192);  // Red
                }
            }
            else
            {
                // Open com port
                if (c_SerialPort_SD.OpenComPort(cbSDComportList.Text, sd_buadrate))
                {
                    c_MotorControl_SD = new Class_MotorControl(c_SerialPort_SD._serialPort);
                    cbSDComportList.Enabled = false;
                    btnSDconnect.Text = "Disconnect";
                    SD_button_connect_status = true;
                    lbSDState.BackColor = Color.FromArgb(192, 255, 192);  // Green

                    ServoDriver_Init_cmd();  // init
                    //c_Thread_SD_Receive.start();
                }
            }
        }

        private void btnCOMM_Click(object sender, EventArgs e)
        {
            gbCOMM.Visible = true;
            gbManualControl.Visible = false;
            gbAutoControl.Visible = false;
        }

        private void btnManualControl_Click(object sender, EventArgs e)
        {
            gbCOMM.Visible = false;
            gbManualControl.Visible = true;
            gbAutoControl.Visible = false;
        }

        private void btnAutoControl_Click(object sender, EventArgs e)
        {
            gbCOMM.Visible = false;
            gbManualControl.Visible = false;
            gbAutoControl.Visible = true;
        }

        private void gbGenMode_EnabledChanged(object sender, EventArgs e)
        {
            if (gbGenMode.Enabled == false)
            {
                gbGenMode.BackColor = Color.DarkGray;
            }
            else
            {
                gbGenMode.BackColor = Color.Black;
                gbGenMode.ForeColor = Color.Cyan;
            }
        }

        private void gbMotorMode_EnabledChanged(object sender, EventArgs e)
        {
            if (gbMotorMode.Enabled == false)
            {
                gbMotorMode.BackColor = Color.DarkGray;
            }
            else
            {
                gbMotorMode.BackColor = Color.Black;
                gbMotorMode.ForeColor = Color.Lime;
            }
        }

        private void panel1_Scroll(object sender, ScrollEventArgs e)
        {
            panel1.VerticalScroll.Value = e.NewValue;
        }

        private void mointer_refresh_timer_Tick(object sender, EventArgs e)
        {
            // Mointer groupbox ui fresh area
            // for example
            // Vdc_value.Text = s_Mointer.input_vdc.ToString();
            Vdc_value.Text = s_Mointer.input_vdc.ToString();
            Idc_value.Text = s_Mointer.input_idc.ToString();
            InputPower_value.Text = s_Mointer.input_pwr.ToString();

            Vuv_value.Text = s_Mointer.output_vuv.ToString();
            Vvw_value.Text = s_Mointer.output_vvw.ToString();
            Iu_value.Text =  s_Mointer.output_iu.ToString();
            Iw_value.Text =  s_Mointer.output_iw.ToString();
            DrivePower_value.Text = s_Mointer.output_drive_pwr.ToString();

            Torque_value.Text = s_Mointer.motor_torque.ToString();
            Speed_value.Text = s_Mointer.motor_speed.ToString();
            MotorPower_value.Text = s_Mointer.motor_pwr.ToString();

            DriveEff_value.Text = s_Mointer.efficency_drive_eff.ToString();
            MotorEff_value.Text = s_Mointer.efficency_motor_eff.ToString();

            if(PAInitTaskFinishFlag == false)
            {
                btnPAconnect.Enabled = false;
            }
            else
            {
                btnPAconnect.Enabled = true;
            }
        }

        private void rbMotorModeSeleted_CheckedChanged(object sender, EventArgs e)
        {
            if (rbMotorModeSeleted.Checked)
            {
                if (manualRunFlag == false)
                {
                    gbMotorMode.Enabled = true;
                    gbGenMode.Enabled = false;
                }
                else
                {
                    rbMotorModeSeleted.Checked = false;
                }
            }
        }

        private void rbGenModeSeleted_CheckedChanged(object sender, EventArgs e)
        {
            if (rbGenModeSeleted.Checked)
            {
                if (manualRunFlag == false)
                {
                    gbMotorMode.Enabled = false;
                    gbGenMode.Enabled = true;
                }
                else
                {
                    rbGenModeSeleted.Checked = false;
                }
            }
        }

        private void btnSetSP_Click(object sender, EventArgs e)
        {
            if (manual_set_speed_tb.Text != "")
            {
                SetSDSpeed(manual_set_speed_tb.Text);
            }
        }

        private void btnSetTQ_Click(object sender, EventArgs e)
        {
            if (manual_set_torque_tb.Text != "")
            {
                SetSDTorqueNm(Convert.ToDouble(manual_set_torque_tb.Text));
            }
        }

        private void OnlyNumeric_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox tb = (sender as TextBox);
            int selectionStart = tb.SelectionStart;
            string currentText = tb.Text;
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '-' && e.KeyChar != '.') // not Control character or not numeric
            {
                e.Handled = true;
            }
            else if (Convert.ToInt32(e.KeyChar) == 13) //enter
            {
                e.Handled = true;
            }
            else if ((e.KeyChar == '-' && selectionStart != 0) ||           // '-' only can place at first position
                (e.KeyChar == '-' && currentText.IndexOf('-') != -1) ||     // only one '-'
                (e.KeyChar == '.' && currentText.IndexOf('.') != -1))       // only one '.'
            {
                e.Handled = true;
            }
            else if (currentText.Contains('-') && selectionStart == 0)      // if has '-', nothing can place before it
            {
                e.Handled = true;
            }

        }

        private void btnCatchData_Click(object sender, EventArgs e)
        {
            rtbManualLOG.AppendText(
                string.Format("{0:00.00}", s_Mointer.input_vdc) + ", " + string.Format("{0:00.00}", s_Mointer.input_idc) + ", " +
                string.Format("{0:0000.00}", s_Mointer.input_pwr) + ", " +

                string.Format("{0:00.00}", s_Mointer.output_vuv) + ", " + string.Format("{0:00.00}", s_Mointer.output_vvw) + ", " +
                string.Format("{0:00.00}", s_Mointer.output_iu) + ", " + string.Format("{0:00.00}", s_Mointer.output_iw) + ", " +
                string.Format("{0:0000.00}", s_Mointer.output_drive_pwr) + ", " +

                string.Format("{0:00.00}", s_Mointer.motor_torque) + ", " + string.Format("{0:000.0}", s_Mointer.motor_speed) + ", " +
                string.Format("{0:0000.00}", s_Mointer.motor_pwr) + ", " +

                string.Format("{0:00.00}", s_Mointer.efficency_drive_eff) + ", " + string.Format("{0:00.00}", s_Mointer.efficency_motor_eff) + ", " +
                Environment.NewLine);
        }

        private void rtbManualLOG_TextChanged(object sender, EventArgs e)
        {
            rtbManualLOG.SelectionStart = rtbManualLOG.Text.Length;
            rtbManualLOG.ScrollToCaret();  // display last line
        }

        private void btnSaveFile_Click(object sender, EventArgs e)
        {
            SaveCsvFile(rtbManualLOG.Text);
        }
        private void manual_stop_btn_Click(object sender, EventArgs e)
        {

            if (rbMotorModeSeleted.Checked)
            {
                SetSDSpeed("0");
                //Thread.Sleep(1000);
            }
            c_MotorControl_SD.Stop();
            manual_start_btn.Enabled = true;
            manual_stop_btn.Enabled = false;
            manualRunFlag = false;
        }

        private void manual_start_btn_Click(object sender, EventArgs e)
        {
            if (c_MotorControl_SD.SetScurve(1000, 1000, 0)) // set TACC: 1000 ms, TDEC: 1000 ms, TSL: 0 ms
            {
                if (rbMotorModeSeleted.Checked)
                {
                    c_MotorControl_SD.SetTorquePercent(200);  // set TQ1 : 200 % 
                    if (manual_set_speed_tb.Text != "")
                    {
                        SetSDSpeed(manual_set_speed_tb.Text);
                    }
                }
                else if (rbGenModeSeleted.Checked)
                {
                    SetSDSpeed("0");  // set SP1:0 rpm
                    if (manual_set_torque_tb.Text != "")
                    {
                        SetSDTorqueNm(Convert.ToDouble(manual_set_torque_tb.Text));  //N.m
                    }
                }

                c_MotorControl_SD.Run();
                manual_start_btn.Enabled = false;
                manual_stop_btn.Enabled = true;
                manualRunFlag = true;
            }
            else
            {
                MessageBox.Show("Please check comport");
            }

        }
        private void btnAutoStart_Click(object sender, EventArgs e)
        {
            int stepNum = 0;

            if(plotform == null || plotform.IsDisposed)  // 若plotForm被關掉或沒開過
            {
                plotform = new PlotForm();
                plotform.Show();
            } 

            try
            {
                samplingTime = Convert.ToDouble(tbSampleTime.Text) * 1000;  // sec to ms
                if(samplingTime < 200)
                {
                    samplingTime = 200;
                    tbSampleTime.Text = "0.2";
                }
                c_Thread_send_LOG.start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            for (int i = 0; i < 20; i++)
            {
                CheckBox chk = this.Controls.Find("chk_"+(i+1).ToString(),true).FirstOrDefault() as CheckBox;
                if(chk.Checked == true)
                {
                    
                    AutoStepParamList[i].value = GetTbValue(i * 3 + 1);
                    AutoStepParamList[i].stepTime = GetTbValue(i * 3 + 2);
                    AutoStepParamList[i].stableTime = GetTbValue(i * 3 + 3);
                    AutoStepParamList[i].unit = GetCboValue(i + 1);
                    stepNum++;
                }
            }
            if(stepNum != 0)
                AutoControlCmd(stepNum-1);

            btnAutoStart.Enabled = false;
            btnAutoStop.Enabled = true;
        }

        private void btnAutoStop_Click(object sender, EventArgs e)
        {
            if (Thread_AutoControl != null)
                Thread_AutoControl.Abort();

            SetSDSpeed("0");
            //Thread.Sleep(1000);
            c_MotorControl_SD.Stop();
            c_Thread_send_LOG.stop();
            btnAutoStart.Enabled = true;
            btnAutoStop.Enabled = false;
        }

        private void btnClearAll_Click(object sender, EventArgs e)
        {
            rtbManualLOG.Clear();
        }
    }
}
