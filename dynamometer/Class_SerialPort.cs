﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dynamometer
{
    class Class_SerialPort
    {
        public SerialPort _serialPort { get; private set; } = new SerialPort();
        public bool sending { get; private set; } = false;

        public Class_SerialPort()
        {

        }
        public bool OpenComPort(String comport, int baudrate, int databits = 8, StopBits stopbits = StopBits.One)
        {
            try
            {
                if ((_serialPort != null) && (_serialPort.IsOpen) && (!sending))
                {
                    _serialPort.Close();
                }

                _serialPort.PortName = comport;
                _serialPort.BaudRate = baudrate;
                _serialPort.DataBits = databits;
                _serialPort.StopBits = stopbits;

                if ((_serialPort != null) && (!_serialPort.IsOpen))
                {
                    _serialPort.Open();
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool CloseComPort()
        {
            try
            {
                if (_serialPort != null)
                {
                    _serialPort.Close();
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        public void SendData(string data)
        {
            sending = true;
            try
            {
                if(_serialPort.IsOpen)
                    _serialPort.WriteLine(data);
            }
            catch (Exception ex)
            {
                //這邊你可以自訂發生例外的處理程序
                CloseComPort();
                MessageBox.Show(String.Format("Error:{0}", ex.ToString()));
            }
            finally
            {
                sending = false;
            }
        }
        public void SendData(byte[] data)
        {
            sending = true;
            try
            {
                if(_serialPort.IsOpen)
                {
                    //_serialPort.Write(data + "\r\n");
                    _serialPort.Write(data, 0, data.Length);
                    _serialPort.Write(_serialPort.NewLine);  // Add CrLf
                }               
            }
            catch (Exception ex)
            {
                //這邊你可以自訂發生例外的處理程序
                CloseComPort();
                MessageBox.Show(String.Format("Error:{0}", ex.ToString()));
            }
            finally
            {
                sending = false;
            }
        }
        public byte[] ReadData()
        {
            int buffer_len = _serialPort.BytesToRead;
            byte[] output = new byte[buffer_len];
            try
            {
                _serialPort.Read(output, 0, buffer_len);
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Error:{0}", ex.ToString()));
            }
            return output;
        }

        public string ReadExistingData()
        {
            string output = " "; 
            try
            {
                output = _serialPort.ReadExisting();
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format("Error:{0}", ex.ToString()));
            }
            return output;
        }
    }
}
