﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dynamometer
{
    public partial class MainForm : Form
    {
        TextBox textBox1;
        CheckBox checkBox1;
        ComboBox comboBox1;
        public static PlotForm plotform = null;
        string pa_receive_buf_str;
        string[] pa_receive_buf_str_split = new string[500];
        //string[] tm_receive_buf_str_split = new string[500];
        bool manualRunFlag = false;
        bool PAInitTaskFinishFlag = true;
        double samplingTime;

        S_Mointer s_Mointer = new S_Mointer();
        AutoStepParam[] AutoStepParamList = new AutoStepParam[20]; 

        // Baud rate
        const int tm_buadrate = 19200;
        const int pa_buadrate = 38400;
        const int sd_buadrate = 38400;

        // Command 
        string cmdPAInit = ":WIRing TYPE 3";

        string cmdPASetVolt1 = ":VOLT1:RANGE 60";
        string cmdPASetVolt2 = ":VOLT2:RANGE 60";
        string cmdPASetVolt3 = ":VOLT3:RANGE 60";

        string cmdPASetCurr1 = ":CURR1:RANGE 50";
        string cmdPASetCurr2 = ":CURR2:RANGE 50";
        string cmdPASetCurr3 = ":CURR3:RANGE 50";

        string cmdPASetFreq1 = ":FREQ1:RANG 200.0E+3";
        string cmdPASetFreq2 = ":FREQ2:RANG 200.0E+3";

        byte[] cmdSDInit = { 0x01, 0x06, 0x03, 0x0C, 0x00, 0xFF, 0x09, 0xCD };  // (SDI): 0xFF
        byte[] cmdSDUnsaved = { 0x01, 0x06, 0x02, 0x3C, 0x00, 0x05, 0x88, 0x7D };  // (INH): 0x05
        // Command End

        // Thread
        Thread Thread_PA_Init;
        Thread Thread_AutoControl;

        Class_SerialPort c_SerialPort_TM = new Class_SerialPort();
        Class_SerialPort c_SerialPort_PA = new Class_SerialPort();
        Class_SerialPort c_SerialPort_SD = new Class_SerialPort();

        Class_Thread c_Thread_TM_Send = new Class_Thread();
        Class_Thread c_Thread_PA_Send = new Class_Thread();

        Class_Thread c_Thread_TM_Receive = new Class_Thread();
        Class_Thread c_Thread_PA_Receive = new Class_Thread();
        Class_Thread c_Thread_SD_Receive = new Class_Thread();

        Class_Thread c_Thread_send_LOG = new Class_Thread();

        Class_MotorControl c_MotorControl_SD = new Class_MotorControl();
        // Thread End
        struct S_Mointer
        {
            public double input_vdc;
            public double input_idc;
            public double input_pwr;

            public double output_vuv;
            public double output_vvw;
            public double output_iu;
            public double output_iw;
            public double output_drive_pwr;

            public double motor_torque;
            public double motor_speed;
            public double motor_pwr;

            public double efficency_drive_eff;
            public double efficency_motor_eff;
        }

        struct AutoStepParam
        {
            public string value;
            public string unit;
            public string stepTime;
            public string stableTime;
        }
    }
}
